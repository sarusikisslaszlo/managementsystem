package hu.realertek.managementSystem;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@SpringBootConfiguration
class ManagementSystemApplicationTests {

}
