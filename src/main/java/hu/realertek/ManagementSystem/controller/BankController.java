package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.dtos.BankDto;
import hu.realertek.ManagementSystem.service.interf.BankService;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
@RequestMapping("/real")
public class BankController {

  private static final String CONTENT_NAME_ADD_BANK = "add_bank";

  @Autowired
  private CaseService caseService;

  @Autowired
  private BankService bankService;

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/add/bank")
  public String addNewBank(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("bank", new BankDto());
    model.addAttribute("contentFile", CONTENT_NAME_ADD_BANK);
    model.addAttribute("contentName", CONTENT_NAME_ADD_BANK);
    return "index";
  }

  @RequestMapping(value = "/add_bank", method = RequestMethod.POST)
  public String savePreparer(@Valid BankDto bankDto, BindingResult bindinresult, Model model) throws IOException {
    if (bindinresult.hasErrors()) {
      addNewBank(model);
    }

    String fileName = bankService.save(bankDto).getFileName();

    String uploadDir = "C:/kozos/ManagementSystem/bank_kepek/" + bankDto.getName();  //  String uploadDir = "/Users/macbook/test/" + bankDto.getName(); String uploadDir = "C:/kozos/ManagementSystem/bank_kepek/" + bankDto.getName();

    Path uploadPath = Paths.get(uploadDir);

    if(!Files.exists(uploadPath)) {
      Files.createDirectories(uploadPath);
    }

    try(InputStream inputStream = bankDto.getFile().getInputStream()) {
      Path filePath = uploadPath.resolve(fileName);
      Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
    } catch (Exception e) {
      throw new IOException("Valami probléma lépett fel a kép mentése során!");
    }

    return "redirect:/real/list";
  }

}
