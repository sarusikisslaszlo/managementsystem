package hu.realertek.ManagementSystem.controller;

import static guru.nidi.graphviz.model.Factory.mutGraph;
import static guru.nidi.graphviz.model.Factory.node;
import static guru.nidi.graphviz.model.Link.to;

import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.Shape;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.Node;
import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class VisualController {

  @Autowired
  private  StateMachineFactory<RealState, RealEvent> stateMachineFactory;
  private Graphviz graphviz;

  @PostConstruct
  public void init() throws Exception {
    StateMachine<RealState, RealEvent> stateMachine = stateMachineFactory.getStateMachine();
    MutableGraph graph = mutGraph("Order State Machine").setDirected(true);

    Map<String, Set<GraphVizLink>> graphMap = new HashMap<>();
    stateMachine.getStates().forEach(
        s -> graphMap.put(s.getId().name(), new HashSet<>())
                                    );

    stateMachine.getTransitions().forEach(t -> {
      graphMap.get(t.getSource().getId().name()).add(
          new GraphVizLink(t.getTarget().getId().name(), t.getTrigger().getEvent().name())
                                                    );
    });

    graphMap.forEach((key, targetSet) -> {
      Node node = node(key).with(Shape.RECTANGLE);
      targetSet.forEach(l -> graph.add(
          node.link(to(node(l.getTarget())).with(Label.of(l.getLabel().toLowerCase())))
                                      ));
    });

    this.graphviz = Graphviz.fromGraph(graph);
  }

  @ResponseBody
  @RequestMapping(value = "/visualize", consumes = MediaType.ALL_VALUE, produces = "image/svg+xml")
  public String renderSVG() {
    // please install library on host OS: https://graphviz.org/download/
    // sudo apt install graphviz
    return graphviz.render(Format.SVG).toString();
  }

  @Data
  private static class GraphVizLink {

    private final String target;
    private final String label;
  }

}
