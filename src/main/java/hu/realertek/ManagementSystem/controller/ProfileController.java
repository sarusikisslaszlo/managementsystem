package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.service.interf.JobService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/real")
public class ProfileController {

  private static final String CONTENT_NAME_PROFILE = "profile";
  private static final Object CONTENT_NAME_ACTIVITY = "activity";

  @Autowired
  private CaseService caseService;

  @Autowired
  private UserService userService;

  @Autowired
  private JobService jobService;

  /**
	 * 
	 * @param model
	 * @param authentication
	 */
	@RequestMapping("/profile")
  public String getUserDetails(Model model, Authentication authentication) {
    UserDetails userDetails = (UserDetails) authentication.getPrincipal();
    UsersDto usersDto = userService.getDtoByEntity(userService.getByUserName(userDetails.getUsername()));
    model.addAttribute("user", usersDto);
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("contentFile", CONTENT_NAME_PROFILE);
    model.addAttribute("contentName", CONTENT_NAME_PROFILE);
    return "index";
  }

  /**
	 * 
	 * @param usersDto
	 * @param bindingResult
	 * @param model
	 */
	@RequestMapping("/update_user")
  public String updateUserDetails(@Valid UsersDto usersDto, BindingResult bindingResult, Model model) throws Exception {
    if (bindingResult.hasErrors()) {
      throw new Exception("Something went wrong during saving new user!");
    }

    userService.update(usersDto);

    return "redirect:/real/profile";
  }

  /**
	 * 
	 * @param model
	 * @param authentication
	 */
	@RequestMapping("/activity")
  public String getUserActivity(Model model, Authentication authentication) {
    UserDetails userDetails = (UserDetails) authentication.getPrincipal();
    UserEntity userEntity = userService.getByUserName(userDetails.getUsername());
    model.addAttribute("user", userEntity);
    model.addAttribute("activity", caseService.findAllByPreparerOrValuerWhereState(userEntity));
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("contentFile", CONTENT_NAME_ACTIVITY);
    model.addAttribute("contentName", CONTENT_NAME_ACTIVITY);
    return "index";
  }

}
