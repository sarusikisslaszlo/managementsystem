package hu.realertek.ManagementSystem.controller;

import com.lowagie.text.DocumentException;
import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import hu.realertek.ManagementSystem.dtos.ConfigDto;
import hu.realertek.ManagementSystem.dtos.PdfDto;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.service.interf.ConfigService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import hu.realertek.ManagementSystem.settings.PdfService;
import hu.realertek.ManagementSystem.settings.RealExcelExporter;
import hu.realertek.ManagementSystem.statemachine.Workflow;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
@RequestMapping("/real")
public class SystemController {

  private static final String CONTENT_NAME_LIST_DONE = "done";
  private static final String CONTENT_NAME_LIST_INPROGRESS = "in_progress";
  private static final String CONTENT_NAME_SUCCESS = "success";
  private static final String CONTENT_NAME_PDF = "pdf_generating";
  private static final String CONTENT_NAME_CONFIG = "config";

  @Autowired
  private Workflow workflow;

  @Autowired
  private CaseService caseService;

  @Autowired
  private UserService userService;

  @Autowired
  private PdfService pdfService;

  @Autowired
  private RealExcelExporter realExcelExporter;

  @Autowired
  private ConfigService configService;

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/add/config")
  public String addNewConfig(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("config", new ConfigDto());
    model.addAttribute("contentFile", CONTENT_NAME_CONFIG);
    model.addAttribute("contentName", CONTENT_NAME_CONFIG);
    return "index";
  }

  /**
	 * 
	 * @param configDto
	 * @param bindingResult
	 * @param model
	 */
	@RequestMapping("/save_config")
  public String updateUserDetails(@Valid ConfigDto configDto, BindingResult bindingResult, Model model) throws Exception {
    if (bindingResult.hasErrors()) {
      throw new Exception("Something went wrong during saving new config!");
    }

    configService.save(configDto);

    return "redirect:/real/add/config";
  }

  /**
	 * 
	 * @param id
	 * @param event
	 */
	@GetMapping("/show/trigger/{id}")
  public String triggerEvent(@PathVariable("id") Long id, @RequestParam("event") RealEvent event) {
    StateMachine<RealState, RealEvent> sm = workflow.build(id);
    boolean accepted = workflow.sendEvent(id, sm, event);
    return "redirect:/real/show/" + id + "?accepted=" + accepted;
  }

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/list_done")
  public String listByStateDone(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("real", caseService.getByStateDone());
    model.addAttribute("contentFile", CONTENT_NAME_LIST_DONE);
    model.addAttribute("contentName", CONTENT_NAME_LIST_DONE);
    return "index";
  }

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/list_inprogress")
  public String listByStateInProgress(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("real", caseService.getByStateInProgress());
    model.addAttribute("contentFile", CONTENT_NAME_LIST_INPROGRESS);
    model.addAttribute("contentName", CONTENT_NAME_LIST_INPROGRESS);
    return "index";
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/success")
  public String success(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("contentFile", CONTENT_NAME_SUCCESS);
    model.addAttribute("contentName", CONTENT_NAME_SUCCESS);
    return "index";
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/pdf-generating")
  public String getPdfGenerating(Model model) {
    model.addAttribute("name", new PdfDto());
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("valuers", userService.getAllValuerByJob());
    model.addAttribute("contentFile", CONTENT_NAME_PDF);
    model.addAttribute("contentName", CONTENT_NAME_PDF);
    return "index";
  }

  @RequestMapping(value = "/pdf", method=RequestMethod.POST)
  public void downloadPDFResource(@Valid PdfDto pdfDto, HttpServletResponse response, @RequestParam(required=false, value="pdf") String pdf,
      @RequestParam(required=false, value="excel") String excel) throws IOException {
    Long id = userService.getIdByUserFirstAndLastName(pdfDto.getName());
    if(pdf != null) {
      try {
        Path file = Paths.get(pdfService.generatePdf(id, pdfDto.getStart(), pdfDto.getEnd()).getAbsolutePath());
        if (Files.exists(file)) {
          response.setContentType("application/pdf");
          response.addHeader("Content-Disposition",
              "attachment; filename=" + file.getFileName());
          Files.copy(file, response.getOutputStream());
          response.getOutputStream().flush();
        }
      } catch (DocumentException | IOException ex) {
        ex.printStackTrace();
      }
    } else {
      response.setContentType("application/octet-stream");
      DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
      String currentDateTime = dateFormatter.format(new Date());

      List<CaseEntity> caseEntities = caseService.getCasesByValuerForCurrentMonth(id, pdfDto.getStart(), pdfDto.getEnd());

      String headerKey = "Content-Disposition";
      String headerValue;
      if(caseEntities.size() > 0) {
        headerValue = "attachment; filename=" + caseEntities.get(0).getValuer().getName() + "_havi_elszamolas_" + currentDateTime + ".xlsx";
      } else {
        headerValue = "attachment; filename=üres_havi_elszamolas_" + currentDateTime + ".xlsx";
      }
      response.setHeader(headerKey, headerValue);

      realExcelExporter.exportUserMonthlySum(caseEntities, response);
    }
  }

  /**
	 * 
	 * @param response
	 */
	@GetMapping("/pdf-statistics")
  public void downloadPDFResourceForStatistics(HttpServletResponse response) throws IOException {
    try {
      Path file = Paths.get(pdfService.generatePdfForStatistics().getAbsolutePath());
      if (Files.exists(file)) {
        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition",
            "attachment; filename=" + file.getFileName());
        Files.copy(file, response.getOutputStream());
        response.getOutputStream().flush();
      }
    } catch (DocumentException | IOException ex) {
      ex.printStackTrace();
    }
  }
}
