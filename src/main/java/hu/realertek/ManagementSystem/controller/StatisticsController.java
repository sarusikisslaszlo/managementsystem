package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.dtos.ConfigDto;
import hu.realertek.ManagementSystem.dtos.CustomStatisticsDto;
import hu.realertek.ManagementSystem.service.interf.BankService;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import java.math.BigInteger;
import java.util.List;

import hu.realertek.ManagementSystem.service.interf.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/real")
public class StatisticsController {

  private static final String CONTENT_NAME_STATISTICS = "statistics";
  private static final String CONTENT_NAME_CUSTOM_DATE_STATISTICS = "custom_date_statistics";
  private static final String CONTENT_NAME_EVALUATED_STATISTICS = "evaluated_statistics";

  @Autowired
  private UserService userService;

  @Autowired
  private CaseService caseService;

  @Autowired
  private BankService bankService;

  private int[] getDataForChartInteger(List<Object[]> object) {
    int[] tomb = new int[object.size()];
    for (int i = 0; i < object.size(); i++) {
      Object[] tomb2 = object.get(i);
      tomb[i] = ((BigInteger) tomb2[0]).intValue();
    }
    return tomb;
  }

  private String[] getDataForChartString(List<Object[]> object) {
    String[] tomb2 = new String[object.size()];
    for (int i = 0; i < object.size(); i++) {
      Object[] tomb = object.get(i);
      tomb2[i] = tomb[1].toString();
    }
    return tomb2;
  }

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/statistics")
  public String getStatistics(Model model) {
      model.addAttribute("prio", caseService.getByPriority());
      model.addAttribute("circle", caseService.getCaseCountGroupByBankName());

      model.addAttribute("line_year", caseService.getStatForLastYear());

      model.addAttribute("line_six", caseService.getStatForLastSixMonth());

      model.addAttribute("line_three", caseService.getStatForLastThreeMonth());

      model.addAttribute("chartCurrentMonthInteger", getDataForChartInteger(caseService.getStatForCurrentMonth()));
      model.addAttribute("chartCurrentMonthString", getDataForChartString(caseService.getStatForCurrentMonth()));
      model.addAttribute("chartLastThreeMonthInteger", getDataForChartInteger(caseService.getStatForLastThreeMonth()));
      model.addAttribute("chartLastThreeMonthString", getDataForChartString(caseService.getStatForLastThreeMonth()));
      model.addAttribute("chartLastSixMonthInteger", getDataForChartInteger(caseService.getStatForLastSixMonth()));
      model.addAttribute("chartLastSixMonthString", getDataForChartString(caseService.getStatForLastSixMonth()));
      model.addAttribute("chartLastYearInteger", getDataForChartInteger(caseService.getStatForLastYear()));
      model.addAttribute("chartLastYearString", getDataForChartString(caseService.getStatForLastYear()));

      model.addAttribute("donutString", getDataForChartString(caseService.getCaseCountGroupByBankName()));
      model.addAttribute("donutInteger", getDataForChartInteger(caseService.getCaseCountGroupByBankName()));

      model.addAttribute("count", caseService.getCount());
      model.addAttribute("amount", caseService.getAmount());
      model.addAttribute("moneyForCurrentMonth", caseService.moneyForCurrentMonth());
      model.addAttribute("caseNumberForCurrentMonth", caseService.getCaseNumberForCurrentMonth());

      model.addAttribute("iktatva", caseService.getIktatva());
      model.addAttribute("visszaigazolt", caseService.getVisszaigazolt());
      model.addAttribute("szemlezve", caseService.getSzemlezve());
      model.addAttribute("elokeszitve", caseService.getElokeszitve());
      model.addAttribute("ertekelesKesz", caseService.getErtekelesKesz());

      model.addAttribute("contentFile", CONTENT_NAME_STATISTICS);
      model.addAttribute("contentName", CONTENT_NAME_STATISTICS);
      return "index";
  }

  /**
   *
   * @param model
   */
  @GetMapping("/statistics/custom")
  public String getCustomStatistics(Model model, CustomStatisticsDto customStatisticsDto) {
    model.addAttribute("prio", caseService.getByPriority());
      model.addAttribute("valuers", userService.getAllValuerByJob());
      model.addAttribute("bank", bankService.findAll());

    model.addAttribute("settings", new CustomStatisticsDto());

    model.addAttribute("contentFile", CONTENT_NAME_CUSTOM_DATE_STATISTICS);
    model.addAttribute("contentName", CONTENT_NAME_CUSTOM_DATE_STATISTICS);
    return "index";
  }

    @RequestMapping("/evaluate_statistics")
    public String evaluateStatistics(@Valid CustomStatisticsDto customStatisticsDto, BindingResult bindingResult, Model model) throws Exception {
        if (bindingResult.hasErrors()) {
            throw new Exception("Something went wrong during saving new config!");
        }

        return "redirect:/real/statistics/evaluated/" + customStatisticsDto.getStart() + "/"
                + customStatisticsDto.getEnd() + "/" + customStatisticsDto.getValuer() + "/"
                + customStatisticsDto.getBank();
    }

    /**
     *
     * @param model
     */
    @RequestMapping("/statistics/evaluated/{start}/{end}/{valuer}/{bank}")
    public String getEvaluatedCustomStatistics(@PathVariable("start") String start, @PathVariable("end") String end,
                                               @PathVariable("valuer") String valuer, @PathVariable("bank") String bank,
                                               Model model, CustomStatisticsDto customStatisticsDto) {
        model.addAttribute("prio", caseService.getByPriority());
        model.addAttribute("circle", caseService.getCaseCountGroupByBankNameBetweenDates(start, end));

        model.addAttribute("settings", new CustomStatisticsDto());

        int[] statbetweenDatesInteger = null;
        String[] statbetweenDatesString = null;

        String[] caseCountGroupByBankNameBetweenDatesString = null;
        int[] caseCountGroupByBankNameBetweenDatesInteger = null;

        int countBetweenDates = 0;
        int amountBetweenDates = 0;
        int caseNumberBetweenDates = 0;
        int iktatvaBetweenDates = 0;
        int visszaigazoltBetweenDates = 0;
        int szemlezveBetweenDates = 0;
        int elokeszitveBetweenDates = 0;
        int ertekelesKeszBetweenDates = 0;

        // Az az eset amikor csak a dátumok alapján szeretnénk szűrni
        if (valuer.equals("null") && bank.equals("null")) {
            statbetweenDatesInteger = getDataForChartInteger(caseService.getStatBetweenDates(start, end));
            statbetweenDatesString = getDataForChartString(caseService.getStatBetweenDates(start, end));
            caseCountGroupByBankNameBetweenDatesString = getDataForChartString(caseService.getCaseCountGroupByBankNameBetweenDates(start, end));
            caseCountGroupByBankNameBetweenDatesInteger = getDataForChartInteger(caseService.getCaseCountGroupByBankNameBetweenDates(start, end));
            countBetweenDates = caseService.getCountBetweenDates(start, end);
            amountBetweenDates = caseService.getAmountBetweenDates(start, end);
            caseNumberBetweenDates = caseService.getCaseNumberBetweenDates(start, end);
            iktatvaBetweenDates = caseService.getIktatvaBetweenDates(start, end);
            visszaigazoltBetweenDates = caseService.getVisszaigazoltBetweenDates(start, end);
            szemlezveBetweenDates = caseService.getSzemlezveBetweenDates(start, end);
            elokeszitveBetweenDates = caseService.getElokeszitveBetweenDates(start, end);
            ertekelesKeszBetweenDates = caseService.getErtekelesKeszBetweenDates(start, end);
            // Az az eset amikor dátum és bank van
        } else if (valuer.equals("null")) {
            statbetweenDatesInteger = getDataForChartInteger(caseService.getStatBetweenDatesAndBank(start, end, bank));
            statbetweenDatesString = getDataForChartString(caseService.getStatBetweenDatesAndBank(start, end, bank));
            caseCountGroupByBankNameBetweenDatesString = getDataForChartString(caseService.getCaseCountGroupByBankNameBetweenDatesAndBank(start, end, bank));
            caseCountGroupByBankNameBetweenDatesInteger = getDataForChartInteger(caseService.getCaseCountGroupByBankNameBetweenDatesAndBank(start, end, bank));
            countBetweenDates = caseService.getCountBetweenDatesAndBank(start, end, bank);
            amountBetweenDates = caseService.getAmountBetweenDatesAndBank(start, end, bank);
            caseNumberBetweenDates = caseService.getCaseNumberBetweenDatesAndBank(start, end, bank);
            iktatvaBetweenDates = caseService.getIktatvaBetweenDatesAndBank(start, end, bank);
            visszaigazoltBetweenDates = caseService.getVisszaigazoltBetweenDatesAndBank(start, end, bank);
            szemlezveBetweenDates = caseService.getSzemlezveBetweenDatesAndBank(start, end, bank);
            elokeszitveBetweenDates = caseService.getElokeszitveBetweenDatesAndBank(start, end, bank);
            ertekelesKeszBetweenDates = caseService.getErtekelesKeszBetweenDatesAndBank(start, end, bank);
            bank = bankService.getById(Long.valueOf(bank)).getName();
            // Az az eset amikor értékbecslő és dátumok vannak
        } else if (bank.equals("null")) {
            statbetweenDatesInteger = getDataForChartInteger(caseService.getStatBetweenDatesAndValuer(start, end, valuer));
            statbetweenDatesString = getDataForChartString(caseService.getStatBetweenDatesAndValuer(start, end, valuer));
            caseCountGroupByBankNameBetweenDatesString = getDataForChartString(caseService.getCaseCountGroupByBankNameBetweenDatesAndValuer(start, end, valuer));
            caseCountGroupByBankNameBetweenDatesInteger = getDataForChartInteger(caseService.getCaseCountGroupByBankNameBetweenDatesAndValuer(start, end, valuer));
            countBetweenDates = caseService.getCountBetweenDatesAndValuer(start, end, valuer);
            amountBetweenDates = caseService.getAmountBetweenDatesAndValuer(start, end, valuer);
            caseNumberBetweenDates = caseService.getCaseNumberBetweenDatesAndValuer(start, end, valuer);
            iktatvaBetweenDates = caseService.getIktatvaBetweenDatesAndValuer(start, end, valuer);
            visszaigazoltBetweenDates = caseService.getVisszaigazoltBetweenDatesAndValuer(start, end, valuer);
            szemlezveBetweenDates = caseService.getSzemlezveBetweenDatesAndValuer(start, end, valuer);
            elokeszitveBetweenDates = caseService.getElokeszitveBetweenDatesAndValuer(start, end, valuer);
            ertekelesKeszBetweenDates = caseService.getErtekelesKeszBetweenDatesAndValuer(start, end, valuer);
            valuer = userService.getById(Long.valueOf(valuer)).getName();
        } else {
            statbetweenDatesInteger = getDataForChartInteger(caseService.getStatBetweenDatesAndValuerAndBank(start, end, valuer, bank));
            statbetweenDatesString = getDataForChartString(caseService.getStatBetweenDatesAndValuerAndBank(start, end, valuer, bank));
            caseCountGroupByBankNameBetweenDatesString = getDataForChartString(caseService.getCaseCountGroupByBankNameBetweenDatesAndValuerAndBank(start, end, valuer, bank));
            caseCountGroupByBankNameBetweenDatesInteger = getDataForChartInteger(caseService.getCaseCountGroupByBankNameBetweenDatesAndValuerAndBank(start, end, valuer, bank));
            countBetweenDates = caseService.getCountBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            amountBetweenDates = caseService.getAmountBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            caseNumberBetweenDates = caseService.getCaseNumberBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            iktatvaBetweenDates = caseService.getIktatvaBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            visszaigazoltBetweenDates = caseService.getVisszaigazoltBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            szemlezveBetweenDates = caseService.getSzemlezveBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            elokeszitveBetweenDates = caseService.getElokeszitveBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            ertekelesKeszBetweenDates = caseService.getErtekelesKeszBetweenDatesAndValuerAndBank(start, end, valuer, bank);
            valuer = userService.getById(Long.valueOf(valuer)).getName();
            bank = bankService.getById(Long.valueOf(bank)).getName();
        }

        model.addAttribute("dateFrom", start);
        model.addAttribute("dateTo", end);
        model.addAttribute("searchValuer", valuer);
        model.addAttribute("searchBank", bank);

        model.addAttribute("valuers", userService.getAllValuerByJob());
        model.addAttribute("bank", bankService.findAll());
        model.addAttribute("chartCurrentMonthInteger", statbetweenDatesInteger);
        model.addAttribute("chartCurrentMonthString", statbetweenDatesString);

        model.addAttribute("donutString", caseCountGroupByBankNameBetweenDatesString);
        model.addAttribute("donutInteger", caseCountGroupByBankNameBetweenDatesInteger);

        model.addAttribute("count", countBetweenDates);
        model.addAttribute("amount", amountBetweenDates);
        model.addAttribute("moneyForCurrentMonth", amountBetweenDates);
        model.addAttribute("caseNumberForCurrentMonth", caseNumberBetweenDates);

        model.addAttribute("iktatva", iktatvaBetweenDates);
        model.addAttribute("visszaigazolt", visszaigazoltBetweenDates);
        model.addAttribute("szemlezve", szemlezveBetweenDates);
        model.addAttribute("elokeszitve", elokeszitveBetweenDates);
        model.addAttribute("ertekelesKesz", ertekelesKeszBetweenDates);

        model.addAttribute("contentFile", CONTENT_NAME_EVALUATED_STATISTICS);
        model.addAttribute("contentName", CONTENT_NAME_EVALUATED_STATISTICS);
        return "index";
    }

}
