package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import hu.realertek.ManagementSystem.service.interf.BankService;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.service.interf.ConfigService;
import hu.realertek.ManagementSystem.service.interf.JobService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
@RequestMapping("/real")
public class AdminController {

  private static final String CONTENT_NAME_ADMIN = "admin";
  private static final String CONTENT_NAME_ADMIN_LIST = "admin_list";
  private static final String CONTENT_NAME_ADMIN_USER_LIST = "admin_user";
  private static final String CONTENT_NAME_ADMIN_BANK = "admin_bank";
  private static final String CONTENT_NAME_ADMIN_CONFIG = "admin_config";
  private static final String CONTENT_NAME_ADMIN_USER_EDIT = "update_profile";

  @Autowired
  private JobService jobService;

  @Autowired
  private CaseService caseService;

  @Autowired
  private UserService userService;

  @Autowired
  private BankService bankService;

  @Autowired
  private ConfigService configService;

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/admin")
  public String adminSite(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("users", new UsersDto());
    model.addAttribute("job", jobService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_ADMIN);
    model.addAttribute("contentName", CONTENT_NAME_ADMIN);
    return "index";
  }

  @RequestMapping(value = "/admin/create_user", method = RequestMethod.POST)
  public String saveUser(@Valid UsersDto user, BindingResult bindinresult) throws Exception {
    if (bindinresult.hasErrors()) {
      throw new Exception("Something went wrong during saving new user!");
    }

    userService.saveUser(user);

    return "redirect:/real/success";
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/admin-list")
  public String getAdminList(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("real", caseService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_ADMIN_LIST);
    model.addAttribute("contentName", CONTENT_NAME_ADMIN_LIST);
    return "index";
  }

  @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
  public String deleteCase(@PathVariable Long id) {

    caseService.deleteById(id);

    return "redirect:/real/admin-list";
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/admin-users-list")
  public String getAdminUsersList(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("users", userService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_ADMIN_USER_LIST);
    model.addAttribute("contentName", CONTENT_NAME_ADMIN_USER_LIST);
    return "index";
  }

  /**
	 * 
	 * @param model
	 * @param id
	 */
	@RequestMapping("/admin-user-edit/{id}")
  public String getAdminUserById(Model model, @PathVariable Long id) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("user", userService.getById(id));
    model.addAttribute("job", jobService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_ADMIN_USER_EDIT);
    model.addAttribute("contentName", CONTENT_NAME_ADMIN_USER_EDIT);
    return "index";
  }

  @RequestMapping(value = "/admin/update_user", method = RequestMethod.POST)
  public String updateUser(@Valid UsersDto usersDto, BindingResult bindingResult, Model model) {
    if (bindingResult.hasErrors()) {
      getAdminUserById(model, usersDto.getId());
    }

    UserEntity userEntity = userService.updateUser(usersDto);
    return "redirect:/real/admin-user-edit/" + userEntity.getId();
  }

  @RequestMapping(value = "/delete-user/{id}", method = RequestMethod.GET)
  public String deleteUser(@PathVariable Long id) {
    if(caseService.getByValuer(userService.getById(id)).size() > 0 || caseService.getByPreparer(userService.getById(id)).size() > 0) {
      return "redirect:/real/admin-users-list";
    } else {
      userService.deleteById(id);
      return "redirect:/real/admin-users-list"; 
    }
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/admin-bank-list")
  public String getAdminBankList(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("banks", bankService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_ADMIN_BANK);
    model.addAttribute("contentName", CONTENT_NAME_ADMIN_BANK);
    return "index";
  }

  @RequestMapping(value = "/delete-bank/{id}", method = RequestMethod.GET)
  public String deleteBank(@PathVariable Long id) {
    if(caseService.getByBank(bankService.getById(id)).size() > 0) {
      return "redirect:/real/admin-bank-list";
    } else {
      bankService.deleteById(id);
      return "redirect:/real/admin-bank-list";
    }
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/admin-config-list")
  public String getAdminConfigurationList(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("configs", configService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_ADMIN_CONFIG);
    model.addAttribute("contentName", CONTENT_NAME_ADMIN_CONFIG);
    return "index";
  }

  @RequestMapping(value = "/delete-config/{id}", method = RequestMethod.GET)
  public String deleteConfig(@PathVariable Long id) {
    if(caseService.getByLoan(configService.getById(id)).size() > 0 || caseService.getByEstimation(configService.getById(id)).size() > 0) {
      return "redirect:/real/admin-config-list";
    } else {
      configService.delete(id);
      return "redirect:/real/admin-config-list";
    }
  }
}
