package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.ConfigEntity;
import hu.realertek.ManagementSystem.dtos.CaseDto;
import hu.realertek.ManagementSystem.service.interf.BankService;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.service.interf.ConfigService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import java.time.LocalDate;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
@RequestMapping("/real")
public class CaseController {

  private static final String CONTENT_NAME_LIST = "list";
  private static final String CONTENT_NAME_SHOW_CASE = "show_case";
  private static final String CONTENT_NAME_ADD_CASE = "add_case";
  private static final String CONTENT_NAME_EDIT = "edit_page";

  @Autowired
  private CaseService caseService;

  @Autowired
  private BankService bankService;

  @Autowired
  private UserService userService;

  @Autowired
  private ConfigService configService;

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/list")
  public String listPage(Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("date", LocalDate.now());
    model.addAttribute("real", caseService.findAll());
    model.addAttribute("contentFile", CONTENT_NAME_LIST);
    model.addAttribute("contentName", CONTENT_NAME_LIST);
    return "index";
  }

  /**
	 * 
	 * @param id
	 * @param model
	 */
	@GetMapping("/show/{id}")
  public String showById(@PathVariable("id") Long id, Model model) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("real", caseService.getById(id));
    model.addAttribute("contentFile", CONTENT_NAME_SHOW_CASE);
    model.addAttribute("contentName", CONTENT_NAME_SHOW_CASE);
    return "index";
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/add")
  public String addNewElement(Model model) {
    model.addAttribute("real", new CaseDto());
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("preparer", userService.getAllPreparerByJob());
    model.addAttribute("valuer", userService.getAllValuerByJob());
    model.addAttribute("bank", bankService.findAll());
    model.addAttribute("vtype", configService.vtype());
    model.addAttribute("htype", configService.htype());
    model.addAttribute("contentFile", CONTENT_NAME_ADD_CASE);
    model.addAttribute("contentName", CONTENT_NAME_ADD_CASE);
    return "index";
  }

  @RequestMapping(value = "/save_case", method = RequestMethod.POST)
  public String saveElement(@Valid CaseDto caseDto, BindingResult bindinresult, Model model) {
    if (bindinresult.hasErrors()) {
      addNewElement(model);
    }

    CaseEntity caseEntity = caseService.save(caseDto);

    return "redirect:/real/show/" + caseEntity.getId();
  }

  /**
	 * 
	 * @param model
	 * @param id
	 */
	@RequestMapping("/edit/{id}")
  public String edit(Model model, @PathVariable Long id) {
    model.addAttribute("prio", caseService.getByPriority());
    model.addAttribute("real", caseService.getByIdCaseDto(id));
    model.addAttribute("preparer", userService.getAllPreparerByJob());
    model.addAttribute("valuer", userService.getAllValuerByJob());
    model.addAttribute("bank", bankService.findAll());
    if(caseService.getById(id).getEstimation() != null) {
      model.addAttribute("currentEstimation", configService.getById(caseService.getById(id).getEstimation().getId()));
    } else {
      model.addAttribute("currentEstimation", new ConfigEntity(Long.valueOf("-1"), "", ""));
    }
    model.addAttribute("estimation", configService.vtype());
    if(caseService.getById(id).getLoan() != null) {
      model.addAttribute("currentLoan", configService.getById(caseService.getById(id).getLoan().getId()));
    } else {
      model.addAttribute("currentLoan", new ConfigEntity(Long.valueOf("-1"), "", ""));
    }
    model.addAttribute("loan", configService.htype());
    model.addAttribute("contentFile", CONTENT_NAME_EDIT);
    model.addAttribute("contentName", CONTENT_NAME_EDIT);
    return "index";
  }

  @RequestMapping(value = "/update/case", method = RequestMethod.POST)
  public String editCase(@Valid CaseDto caseDto, BindingResult bindingResult, Model model) {
    if (bindingResult.hasErrors()) {
      edit(model, caseDto.getId());
    }

    caseService.updateCase(caseDto);

    return "redirect:/real/show/" + caseDto.getId();
  }

}
