package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.settings.RealExcelExporter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/real")
public class ExcelController {

  @Autowired
  private CaseService caseService;

  @Autowired
  private RealExcelExporter realExcelExporter;

  /**
	 * 
	 * @param response
	 */
	@GetMapping("/export/excel")
  public void exportToExcel(HttpServletResponse response) throws IOException {
    response.setContentType("application/octet-stream");
    DateFormat dateFormatter = new SimpleDateFormat("yyyy_MM_dd");
    String currentDateTime = dateFormatter.format(new Date());

    String headerKey = "Content-Disposition";
    String headerValue = "attachment; filename=becslesek_" + currentDateTime + ".xlsx";
    response.setHeader(headerKey, headerValue);

    List<CaseEntity> caseEntities = caseService.findAll();

    realExcelExporter.exportToResponse(caseEntities, response);
  }

  /*@GetMapping("/export/excel/state/done")
  public void exportToExcelByDone(HttpServletResponse response) throws IOException {
    response.setContentType("application/octet-stream");
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    String currentDateTime = dateFormatter.format(new Date());

    String headerKey = "Content-Disposition";
    String headerValue = "attachment; filename=kesz_becslesek_" + currentDateTime + ".xlsx";
    response.setHeader(headerKey, headerValue);

    List<CaseEntity> realEntities = caseService.findAll();

    realExcelExporter.exportToResponse(realEntities, response);
  }*/

}
