package hu.realertek.ManagementSystem.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
public class AuthController {

  @GetMapping("/login")
  public String login() {
    return "login";
  }

  /**
	 * 
	 * @param model
	 */
	@RequestMapping("/login?error")
  public String loginError(Model model) {
    model.addAttribute("loginError", true);
    return "login";
  }

}
