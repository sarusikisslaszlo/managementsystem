package hu.realertek.ManagementSystem.controller;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.service.general.ForgotPasswordService;
import hu.realertek.ManagementSystem.utils.Utility;
import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
public class ForgotPasswordController {

  @Autowired
  private JavaMailSender mailSender;

  @Autowired
  private ForgotPasswordService forgotPasswordService;

  @GetMapping("/forgot_password")
  public String showForgotPasswordForm() {
    return "forgot_password";
  }

  @RequestMapping(value = "/forgot", method = RequestMethod.POST)
  public String processForgotPassword(HttpServletRequest request, Model model) {
    String email = request.getParameter("email");
    String token = RandomString.make(30);

    try {
      forgotPasswordService.updateResetPasswordToken(token, email);
      String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password/?token=" + token;
      sendEmail(email, resetPasswordLink);
      model.addAttribute("message", "Elküldtük a linket a jelszó megváltoztatásához, ellenőrizd email fiókodat.");
    } catch (Exception ex) {
      model.addAttribute("error", ex.getMessage());
    }
    return "message";
  }

  private void sendEmail(String recipientEmail, String link)
      throws MessagingException, UnsupportedEncodingException, MailException {
    MimeMessage message = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message);

    helper.setFrom("realertekapp@yahoo.com", "REÁL ÉRTÉK Kft.");
    helper.setTo(recipientEmail);

    String subject = "Jelszó változtató link";

    String content = "<p>Kedves felhasználó,</p>"
        + "<p>Ez az üzenet a jelszó változtatási kérelmed miatt érkezett.</p>"
        + "<p>Kattints az alábbi linkre a jelszavad megváltoztatásához:</p>"
        + "<p><a href=\"" + link + "\">Megváltoztatom a jelszavamat</a></p>"
        + "<br>"
        + "<p>Ha emlékszel a jelszavadra, vagy nem te küldted a jelszó változtatási kélemet,"
        + " akkor hagy ezt az üzenetet figyelmen kívül.</p>";


    helper.setSubject(subject);

    helper.setText(content, true);
    mailSender.send(message);
  }


  /**
	 * 
	 * @param token
	 * @param model
	 */
	@RequestMapping(value="/reset_password/", method=RequestMethod.GET)
  public String showResetPasswordForm(@RequestParam("token") String token, Model model) {
    UserEntity userEntity = forgotPasswordService.getByResetPasswordToken(token);
    model.addAttribute("token", token);

    if (userEntity == null) {
      model.addAttribute("message", "Invalid Token");
      return "message";
    }

    return "reset_password";
  }

  @RequestMapping(value = "/reset", method = RequestMethod.POST)
  public String processResetPassword(HttpServletRequest request, Model model) {
    String token = request.getParameter("token");
    String password = request.getParameter("psw");

    UserEntity userEntity = forgotPasswordService.getByResetPasswordToken(token);
    model.addAttribute("title", "Változtasd meg a jelszavadat!");

    if (userEntity == null) {
      model.addAttribute("message", "Invalid Token");
      return "message";
    } else {
      forgotPasswordService.updatePassword(userEntity, password);

      model.addAttribute("message", "Sikeresen jelszót változtattál.");
    }

    return "message";
  }

}
