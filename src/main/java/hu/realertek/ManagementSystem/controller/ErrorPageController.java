package hu.realertek.ManagementSystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorPageController {

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/403")
  public String forbidden(Model model) {
    return "error/403";
  }

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/404")
  public String notFound(Model model) {
    return "error/404";
  }

  /**
	 * 
	 * @param model
	 */
	@GetMapping("/500")
  public String internal(Model model) {
    return "error/500";
  }

  @GetMapping("/access-denied")
  public String accessDenied() {
    return "error/403";
  }
}
