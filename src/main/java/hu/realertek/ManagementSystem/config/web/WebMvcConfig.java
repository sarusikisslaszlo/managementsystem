package hu.realertek.ManagementSystem.config.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {

    registry.addResourceHandler("/Users/macbook/test/**")
            .addResourceLocations("file:/Users/macbook/test/");
    registry.addResourceHandler("Excel/**")
            .addResourceLocations("file:/Users/macbook/test/Excel/");

  }
}
