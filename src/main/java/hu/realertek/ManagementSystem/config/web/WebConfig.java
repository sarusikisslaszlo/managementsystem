package hu.realertek.ManagementSystem.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    super.addViewControllers(registry);
    registry.addViewController("/").setViewName("forward:/login");
    registry.addViewController("/login").setViewName("login");
    registry.addViewController("/reset_password").setViewName("reset_password");
    registry.addViewController("/forgot_password").setViewName("forgot_password");
    registry.addViewController("/pdf").setViewName("pdf_valuer_report");
    registry.addViewController("/pdf-statistics").setViewName("statistics_report");
    registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(
        "/img/**",
        "/css/**",
        "/templates/**",
        "/kozos/ManagementSystem/bank_kepek/**",
        "/Users/macbook/test/font/**")
            .addResourceLocations(
                "classpath:/static/img/",
                "classpath:/static/css/",
                "classpath:/templates/",
                "file:///C:/kozos/ManagementSystem/bank_kepek/",
                "/Users/macbook/test/font/");
  }

  @Bean
  public ClassLoaderTemplateResolver yourTemplateResolver() {
    ClassLoaderTemplateResolver configurer = new ClassLoaderTemplateResolver();
    configurer.setPrefix("templates/");
    configurer.setSuffix(".html");
    configurer.setTemplateMode(TemplateMode.HTML);
    configurer.setCharacterEncoding("UTF-8");
    configurer.setOrder(0);
    configurer.setCacheable(false);
    configurer.setCheckExistence(true);
    return configurer;
  }
}
