package hu.realertek.ManagementSystem.config.security;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("customUserService")
public class CustomUserDetailService implements UserDetailsService {

  @Autowired
  private UserService userService;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    final UserEntity customer = userService.getByUserName(username);
    if(customer == null){
      throw new UsernameNotFoundException(username);
    }
    return User.withUsername(customer.getUserName()).password(customer.getPassword()).roles(customer.getRole().toUpperCase()).build();
  }
}
