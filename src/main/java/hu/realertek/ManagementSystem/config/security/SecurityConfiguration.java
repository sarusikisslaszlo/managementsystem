package hu.realertek.ManagementSystem.config.security;

import javax.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Resource(name = "customUserService")
  private UserDetailsService userDetailsService;

  @Bean
  public UserDetailsService userDetailsService() {
    return super.userDetailsService();
  }

  @Bean
  public DaoAuthenticationProvider authProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService);
    authProvider.setPasswordEncoder(passwordEncoder());
    return authProvider;
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authProvider());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
        .antMatchers("/templates/login").permitAll()
        .antMatchers("/reset_password/**").permitAll()
        .antMatchers("/forgot_password").permitAll()
        .antMatchers("/forgot").permitAll()
        .antMatchers("/reset").permitAll()
        .antMatchers("/").permitAll()
        .antMatchers("/templates/**").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/bank-logos/**").permitAll()
        .antMatchers("/real/admin").hasRole("ADMIN")
        .antMatchers("/real/pdf-generating").hasRole("ADMIN")
        .antMatchers("/templates/pdf_valuer_report.html").hasRole("ADMIN")
        .antMatchers("/templates/statistics_report.html").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/pdf").hasRole("ADMIN")
        .antMatchers("/real/pdf-statistics").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/admin-list").hasRole("ADMIN")
        .antMatchers("/real/delete/**").hasRole("ADMIN")
        .antMatchers("/real/admin-users-list").hasRole("ADMIN")
        .antMatchers("/real/admin-config-list").hasRole("ADMIN")
        .antMatchers("/real/delete-user/**").hasRole("ADMIN")
        .antMatchers("/real/admin-bank-list").hasRole("ADMIN")
        .antMatchers("/real/delete-bank/**").hasRole("ADMIN")
        .antMatchers("/real/list_inprogress").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/list").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/list_done").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/add").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/statistics").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/add/bank").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/real/add/config").hasAnyRole("ADMIN", "EMPLOYEE")
        .antMatchers("/login").permitAll()
        .antMatchers("/error").permitAll()
        .antMatchers("/real/success").hasRole("ADMIN")
        .antMatchers("/css/**")
        .permitAll()
        .antMatchers("/img/**").permitAll()
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage("/login").permitAll()
        .defaultSuccessUrl("/real/list_inprogress", true)
        .and()
        .logout()
        .logoutSuccessUrl("/login?logout")
        .permitAll();
  }
}
