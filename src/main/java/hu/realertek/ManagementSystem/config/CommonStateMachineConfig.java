package hu.realertek.ManagementSystem.config;

import hu.realertek.ManagementSystem.config.actions.DataCollectingAction;
import hu.realertek.ManagementSystem.config.actions.CancelAction;
import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

@Slf4j
public class CommonStateMachineConfig extends StateMachineConfigurerAdapter<RealState, RealEvent> {

  @Autowired
  protected CancelAction cancelAction;
  @Autowired
  protected DataCollectingAction dataCollectingAction;

  @Override
  public void configure(StateMachineConfigurationConfigurer<RealState, RealEvent> config) throws Exception {
    StateMachineListenerAdapter<RealState, RealEvent> adapter = new StateMachineListenerAdapter<>() {
      @Override
      public void stateChanged(State<RealState, RealEvent> from, State<RealState, RealEvent> to) {
        log.info(String.format("stateChanged(from: %s, to: %s)", from.getId(), to.getId()));
      }
    };
    config.withConfiguration().listener(adapter);
  }
}
