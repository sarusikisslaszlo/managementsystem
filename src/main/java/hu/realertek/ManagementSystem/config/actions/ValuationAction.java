package hu.realertek.ManagementSystem.config.actions;

import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("valuationAction")
public class ValuationAction implements Action<RealState, RealEvent> {

  @Override
  public void execute(StateContext<RealState, RealEvent> context) {
    log.info("Valuation action");
  }

}
