package hu.realertek.ManagementSystem.statemachine;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RealStateChangeInterceptor extends StateMachineInterceptorAdapter<RealState, RealEvent> {

  private final CaseService caseService;

  @Override
  public void preStateChange(State<RealState, RealEvent> state, Message<RealEvent> message, Transition<RealState, RealEvent> transition,
      StateMachine<RealState, RealEvent> stateMachine) {

    Optional.ofNullable(message).flatMap(msg -> Optional.ofNullable((Long) msg.getHeaders().getOrDefault(Workflow.ID_HEADER, -1L))).ifPresent( id -> {
      CaseEntity caseEntity = caseService.getById(id);
      caseEntity.setState(state.getId());
      caseService.updateState(state.getId(), id);
    });
  }

}
