package hu.realertek.ManagementSystem.statemachine;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.enums.RealEvent;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Workflow {

  public static final String ID_HEADER = "x-id";
  public static final String DATA_HEADER = "x-data";

  private final CaseService caseService;
  private final StateMachineFactory<RealState, RealEvent> stateMachineFactory;
  private final RealStateChangeInterceptor realStateChangeInterceptor;

  /**
   * Initialize state machine for a specific debt.
   *
   * @param caseId - case id
   * @return {@link StateMachine}
   */
  public StateMachine<RealState, RealEvent> build(Long caseId) {
    CaseEntity caseEntity = caseService.getById(caseId);
    StateMachine<RealState, RealEvent> sm = stateMachineFactory.getStateMachine(String.valueOf(caseEntity.getId()));

    sm.stop();

    sm.getStateMachineAccessor().doWithAllRegions(sma -> {
      sma.addStateMachineInterceptor(realStateChangeInterceptor);
      sma.resetStateMachine(new DefaultStateMachineContext<>(caseEntity.getState(), null, null, null));
    });

    sm.start();
    return sm;
  }

  /**
   * Sending an event via the StateMachine.
   *
   * @param caseId the case Id.
   * @param sm the StateMachine.
   * @param event the event we want to trigger.
   * @return true if the event was successfully sent, false if the transition was not succeeded.
   */
  public boolean sendEvent(Long caseId, StateMachine<RealState, RealEvent> sm, RealEvent event) {
    Message msg = MessageBuilder.withPayload(event).setHeader(ID_HEADER, caseId).build();
    State<RealState, RealEvent> initState = sm.getState();
    sm.sendEvent(msg);
    return !sm.hasStateMachineError() && ObjectUtils.notEqual(initState, sm.getState());
  }

  /**
   * Sending an event via the StateMachine.
   *
   * @param caseId the case Id.
   * @param sm the StateMachine.
   * @param event the event we want to trigger.
   * @return true if the event was successfully sent, false if the transition was not succeeded.
   */
  public boolean sendEventWithData(Long caseId, StateMachine<RealState, RealEvent> sm, RealEvent event, WorkflowData data) {
    Message msg = MessageBuilder.withPayload(event).setHeader(ID_HEADER, caseId).setHeader(DATA_HEADER, data).build();
    State<RealState, RealEvent> initState = sm.getState();
    sm.sendEvent(msg);
    return !sm.hasStateMachineError() && ObjectUtils.notEqual(initState, sm.getState());
  }
}
