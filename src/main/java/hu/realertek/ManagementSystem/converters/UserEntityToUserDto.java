package hu.realertek.ManagementSystem.converters;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserEntityToUserDto implements Converter<UserEntity, UsersDto> {

  @Override
  public UsersDto convert(UserEntity userEntity) {
    UsersDto usersDto = new UsersDto();
    usersDto.setId(userEntity.getId());
    usersDto.setName(userEntity.getName());
    usersDto.setUserName(userEntity.getUserName());
    usersDto.setEmail(userEntity.getEmail());
    usersDto.setPassword(userEntity.getPassword());
    usersDto.setPhoneNumber(userEntity.getPhoneNumber());
    usersDto.setRole(userEntity.getRole());
    usersDto.setJob(userEntity.getJob().getName());
    return usersDto;
  }

}
