package hu.realertek.ManagementSystem.converters;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import hu.realertek.ManagementSystem.service.interf.JobService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDtoToUserEntity implements Converter<UsersDto, UserEntity> {

  @Autowired
  private UserService userService;

  @Autowired
  private JobService jobService;

  @Override
  public UserEntity convert(UsersDto usersDto) {
    UserEntity userEntity = userService.getById(usersDto.getId());
    userEntity.setId(usersDto.getId());
    userEntity.setUserName(usersDto.getUserName());
    userEntity.setName(usersDto.getName());
    userEntity.setEmail(usersDto.getEmail());
    userEntity.setPhoneNumber(usersDto.getPhoneNumber());
    userEntity.setJob(jobService.getById(jobService.getIdByJob(usersDto.getJob())));
    return userEntity;
  }

}
