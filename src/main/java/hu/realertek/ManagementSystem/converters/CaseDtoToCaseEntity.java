package hu.realertek.ManagementSystem.converters;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.dtos.CaseDto;
import hu.realertek.ManagementSystem.service.impl.CaseServiceImpl;
import hu.realertek.ManagementSystem.service.interf.AddressService;
import hu.realertek.ManagementSystem.service.interf.BankService;
import hu.realertek.ManagementSystem.service.interf.ConfigService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CaseDtoToCaseEntity implements Converter<CaseDto, CaseEntity> {

  @Autowired
  private AddressService addressService;

  @Autowired
  private BankService bankService;

  @Autowired
  private UserService userService;

  @Autowired
  private ConfigService configService;

  @Override
  public CaseEntity convert(CaseDto caseDto) {
    CaseEntity caseEntity = new CaseEntity();
    caseEntity.setBank(bankService.getByName(caseDto.getBank()));
    caseEntity.setClientName(caseDto.getClientName());
    caseEntity.setReferenceNumber(caseDto.getReferenceNumber());
    caseEntity.setAddress(caseDto.getAddress());
    CaseServiceImpl.updateUserInCase(caseDto, caseEntity, userService);
    caseEntity.setCreated(LocalDateTime.now());
    caseEntity.setComment(caseDto.getComment());
    caseEntity.setMoney(caseDto.getMoney());
    if(caseDto.getLoan() == -1) {
      caseEntity.setLoan(null);
    } else {
      caseEntity.setLoan(configService.getById(caseDto.getLoan()));
    }
    if(caseDto.getEstimation() == -1) {
      caseEntity.setEstimation(null);
    } else {
      caseEntity.setEstimation(configService.getById(caseDto.getEstimation()));
    }
    return caseEntity;
  }

}
