package hu.realertek.ManagementSystem.converters;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.dtos.CaseDto;
import hu.realertek.ManagementSystem.service.interf.AddressService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CaseEntityToCaseDto implements Converter<CaseEntity, CaseDto> {

  @Autowired
  private AddressService addressService;

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

  String localDateTimeToString(LocalDateTime localDateTime) {
    return localDateTime.format(formatter);
  }

  @Override
  public CaseDto convert(CaseEntity caseEntity) {
    CaseDto caseDto = new CaseDto();
    caseDto.setId(caseEntity.getId());
    caseDto.setState(caseEntity.getState());
    caseDto.setBank(caseEntity.getBank().getName());
    caseDto.setClientName(caseEntity.getClientName());
    caseDto.setReferenceNumber(caseEntity.getReferenceNumber());
    caseDto.setAddress(addressService.getById(caseEntity.getAddress().getId()));
    caseDto.getAddress().setZipCode(caseEntity.getAddress().getZipCode());
    caseDto.getAddress().setStreet(caseEntity.getAddress().getStreet());
    caseDto.getAddress().setDoor(caseEntity.getAddress().getDoor());
    caseDto.setPreparer(caseEntity.getPreparer().getName());
    caseDto.setValuer(caseEntity.getValuer().getName());
    if(caseEntity.getArrived() == null) {
      caseDto.setArrived("");
    } else {
      caseDto.setArrived(localDateTimeToString(caseEntity.getArrived()));
    }
    if(caseEntity.getDeadline() == null) {
      caseDto.setDeadline("");
    } else {
      caseDto.setDeadline(localDateTimeToString(caseEntity.getDeadline()));
    }
    if(caseEntity.getReview() == null) {
      caseDto.setReview("");
    } else {
      caseDto.setReview(localDateTimeToString(caseEntity.getReview()));
    }
    caseDto.setComment(caseEntity.getComment());
    caseDto.setMoney(caseEntity.getMoney());
    return caseDto;
  }

}
