package hu.realertek.ManagementSystem.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "munkakor")
public class JobEntity {

  @Id
  private Long id;

  @Column(name = "elnevezes")
  private String name;
}
