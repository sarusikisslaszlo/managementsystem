package hu.realertek.ManagementSystem.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

@Entity
@Data
@Table(name = "bank")
public class BankEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nev")
  private String name;

  @Column(name = "name")
  private String fileName;

  @Transient
  public String getImagePath() {
    if(fileName == null || name == null) {
      return null;
    } else {
      return "/kozos/ManagementSystem/bank_kepek/" + getName() + "/" + getFileName();  // return "/Users/macbook/test/" + getName() + "/" + getFileName();  return "/kozos/ManagementSystem/bank_kepek/" + getName() + "/" + getFileName();
    }
  }
}
