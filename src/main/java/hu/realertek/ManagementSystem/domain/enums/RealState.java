package hu.realertek.ManagementSystem.domain.enums;

public enum RealState {

  IKTATVA("Iktatva"),
  VISSZAIGAZOLT("Visszaigazolt"),
  SZEMLEZVE("Szemlézve"),
  ELOKESZITVE("Előkészítve"),
  ERTEKELES_KESZ("Értékelés kész"),
  ELKULDVE("Elküldve"),

  AKADALY("Akadályközölt"),
  VISSZAVONT("Visszavonva"),
  TOROLT("Törölve"),

  AKADALY_IKTATVA("Akadályközölt"),
  AKADALY_VISSZAIGAZOLT("Akadályközölt"),
  AKADALY_ELOKESZITVE("Akadályközölt"),
  AKADALY_ERTEKELES_KESZ("Akadályközölt"),
  AKADALY_ELKULDVE("Akadályközölt"),
  AKADALY_SZEMLEZVE("Akadályközölt"),
  VISSZAKULDOTT("Visszaküldött");

  private final String displayValue;

  RealState(String displayValue) {
    this.displayValue = displayValue;
  }

  public String getDisplayValue() {
    return displayValue;
  }
}
