package hu.realertek.ManagementSystem.domain;

import hu.realertek.ManagementSystem.domain.enums.RealState;
import java.time.LocalDateTime;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "becslesek")
public class CaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "iktatoszam")
  private String referenceNumber;

  @Builder.Default
  @Enumerated(EnumType.STRING)
  @Column(name = "allapot")
  private RealState state = RealState.IKTATVA;

  @ManyToOne
  @JoinColumn(name = "bank", referencedColumnName = "id")
  private BankEntity bank;

  @Column(name = "ugyfelnev")
  private String clientName;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "cim", referencedColumnName = "id")
  private AddressEntity address;

  @ManyToOne
  @JoinColumn(name = "elokeszito", referencedColumnName = "id")
  private UserEntity preparer;

  @ManyToOne
  @JoinColumn(name = "ertekelo", referencedColumnName = "id")
  private UserEntity valuer;

  @Column(name = "erkezett")
  @DateTimeFormat(pattern = "yyyy.MM.dd HH:mm")
  private LocalDateTime arrived;

  @Column(name = "hatarido")
  @DateTimeFormat(pattern = "yyyy.MM.dd HH:mm")
  private LocalDateTime deadline;

  @Column(name = "szemle")
  @DateTimeFormat(pattern = "yyyy.MM.dd HH:mm")
  private LocalDateTime review;

  @Column(name = "letrehozva")
  private LocalDateTime created;

  @Column(name = "megjegyzes")
  private String comment;

  @Column(name = "osszeg")
  private int money;

  @ManyToOne
  @JoinColumn(name = "hitel_tipus", referencedColumnName = "id")
  private ConfigEntity loan;

  @ManyToOne
  @JoinColumn(name = "ertekeles_tipus", referencedColumnName = "id")
  private ConfigEntity estimation;
}
