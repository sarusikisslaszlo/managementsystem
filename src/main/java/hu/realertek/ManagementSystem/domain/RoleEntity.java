package hu.realertek.ManagementSystem.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "jogosultsag")
public class RoleEntity {

  @Id
  private Long id;

  @Column(name = "jog")
  private String role;
}
