package hu.realertek.ManagementSystem.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "felhasznalo")
public class UserEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nev")
  private String name;

  @Column(name = "felhasznalonev")
  private String userName;

  @Column(name = "jogosultsag")
  private String role;

  @Column(name = "jelszo")
  private String password;

  @ManyToOne
  @JoinColumn(name = "munkakor", referencedColumnName = "id")
  private JobEntity job;

  @Column(name = "email")
  private String email;

  @Column(name = "telefonszam")
  private String phoneNumber;

  @Column(name = "reset_password_token")
  private String resetPasswordToken;
}
