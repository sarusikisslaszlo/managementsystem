package hu.realertek.ManagementSystem.dtos;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class BankDto {

  private String name;

  private MultipartFile file;

}
