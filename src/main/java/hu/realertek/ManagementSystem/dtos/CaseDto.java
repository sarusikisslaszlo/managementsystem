package hu.realertek.ManagementSystem.dtos;

import com.sun.istack.Nullable;
import hu.realertek.ManagementSystem.domain.AddressEntity;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaseDto {

  private Long id;
  private String referenceNumber;
  private RealState state;
  private String bank;
  private String clientName;
  private AddressEntity address;
  private String preparer;
  private String valuer;
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private String arrived;
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private String deadline;
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private String review;
  private String created;
  private String comment;
  @Nullable
  private int money;
  private Long loan;
  private Long estimation;
}
