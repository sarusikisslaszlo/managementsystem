package hu.realertek.ManagementSystem.dtos;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class CustomStatisticsDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private String start;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private String end;

    private String bank;

    private String valuer;
}
