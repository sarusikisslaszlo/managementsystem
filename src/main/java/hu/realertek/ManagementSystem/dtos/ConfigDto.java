package hu.realertek.ManagementSystem.dtos;

import lombok.Data;

@Data
public class ConfigDto {

  private String type;
  private String value;

}
