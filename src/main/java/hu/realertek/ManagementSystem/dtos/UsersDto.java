package hu.realertek.ManagementSystem.dtos;

import lombok.Data;

@Data
public class UsersDto {

  private Long id;
  private String name;
  private String userName;
  private String role;
  private String password;
  private String job;
  private String email;
  private String phoneNumber;
}
