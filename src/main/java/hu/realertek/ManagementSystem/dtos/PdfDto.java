package hu.realertek.ManagementSystem.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PdfDto {

  private String name;

  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private String start;

  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private String end;

}
