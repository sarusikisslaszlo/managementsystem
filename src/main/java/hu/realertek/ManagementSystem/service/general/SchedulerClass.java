package hu.realertek.ManagementSystem.service.general;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.settings.RealExcelExporter;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SchedulerClass {

  @Autowired
  private RealExcelExporter realExcelExporter;

  @Autowired
  private CaseService caseService;

  @Scheduled(cron = "0 0 20 1/1 * ?")
  public void schedulerMethod() throws IOException {
    log.info("Daily excel file");
    List<CaseEntity> caseEntityList = caseService.findAll();
    realExcelExporter.exportToFileSystem(caseEntityList);
  }
}
