package hu.realertek.ManagementSystem.service.general;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.service.interf.UserService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ForgotPasswordService {

  @Autowired
  private UserService userService;

  public void updateResetPasswordToken(String token, String email) throws Exception {
    UserEntity userEntity = userService.getByEmail(email);
    if (userEntity != null) {
      userEntity.setResetPasswordToken(token);
      userService.save(userEntity);
    } else {
      throw new Exception("Could not find any customer with the email " + email);
    }
  }

  public UserEntity getByResetPasswordToken(String token) {
    return userService.findByResetPasswordToken(token);
  }

  public void updatePassword(UserEntity userEntity, String newPassword) {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String encodedPassword = passwordEncoder.encode(newPassword);
    userEntity.setPassword(encodedPassword);

    userEntity.setResetPasswordToken(null);
    userService.save(userEntity);
  }

}
