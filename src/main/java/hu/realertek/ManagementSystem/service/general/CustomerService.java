package hu.realertek.ManagementSystem.service.general;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import hu.realertek.ManagementSystem.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

  @Autowired
  private UsersRepository usersRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  private UserEntity populateUsersData(final UsersDto usersDto) {
    UserEntity user = new UserEntity();
    user.setUserName(usersDto.getUserName());
    user.setRole(usersDto.getRole());
    user.setPassword(passwordEncoder.encode(usersDto.getPassword()));
    return user;
  }
}
