package hu.realertek.ManagementSystem.service.impl;

import hu.realertek.ManagementSystem.converters.UserDtoToUserEntity;
import hu.realertek.ManagementSystem.converters.UserEntityToUserDto;
import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import hu.realertek.ManagementSystem.repository.UsersRepository;
import hu.realertek.ManagementSystem.service.interf.JobService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UsersRepository userRepository;

  @Autowired
  private JobService jobService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UserDtoToUserEntity userDtoToUserEntity;

  @Autowired
  private UserEntityToUserDto userEntityToUserDto;

  @Override
  public UserEntity getById(Long id) {
    return userRepository.getById(id);
  }

  @Override
  public UserEntity getByUserName(String username) {
    return userRepository.getByUserName(username);
  }

  @Override
  public List<UserEntity> findAll() {
    return userRepository.findAll();
  }

  @Override
  public void deleteById(Long id) {
    userRepository.deleteById(id);
  }

  @Override
  public UserEntity findByResetPasswordToken(String token) {
    return userRepository.findByResetPasswordToken(token);
  }

  @Override
  public UserEntity save(UserEntity userEntity) {
    return userRepository.save(userEntity);
  }

  @Override
  public UserEntity update(UsersDto usersDto) {
    return userRepository.save(userDtoToUserEntity.convert(usersDto));
  }

  @Override
  public UsersDto getDtoByEntity(UserEntity userEntity) {
    return userEntityToUserDto.convert(userEntity);
  }

  @Override
  public UserEntity getByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public UserEntity saveUser(UsersDto usersDto) {
    UserEntity userEntity = new UserEntity();
    userEntity.setName(usersDto.getName());
    userEntity.setEmail(usersDto.getEmail());
    userEntity.setUserName(usersDto.getUserName());
    userEntity.setPhoneNumber(usersDto.getPhoneNumber());
    userEntity.setJob(jobService.getById(jobService.getIdByJob(usersDto.getJob())));
    userEntity.setRole(usersDto.getRole());
    userEntity.setPassword(passwordEncoder.encode(usersDto.getPassword()));
    return userRepository.save(userEntity);
  }

  @Override
  public UserEntity updateUser(UsersDto usersDto) {
    UserEntity userEntity = getById(usersDto.getId());
    userEntity.setName(usersDto.getName());
    userEntity.setEmail(usersDto.getEmail());
    userEntity.setUserName(usersDto.getUserName());
    userEntity.setPhoneNumber(usersDto.getPhoneNumber());
    userEntity.setJob(jobService.getById(jobService.getIdByJob(usersDto.getJob())));
    userEntity.setRole(usersDto.getRole());
    userEntity.setPassword(passwordEncoder.encode(usersDto.getPassword()));
    return userRepository.save(userEntity);
  }

  @Override
  public List<UserEntity> getAllValuerByJob() {
    return userRepository.getAllValuerByJob();
  }

  @Override
  public List<UserEntity> getAllPreparerByJob() {
    return userRepository.getAllPreparerByJob();
  }

  @Override
  public Long getIdByUserFirstAndLastName(String name) {
    return userRepository.getIdByUserFirstAndLastName(name);
  }
}
