package hu.realertek.ManagementSystem.service.impl;

import hu.realertek.ManagementSystem.converters.CaseDtoToCaseEntity;
import hu.realertek.ManagementSystem.converters.CaseEntityToCaseDto;
import hu.realertek.ManagementSystem.domain.BankEntity;
import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.ConfigEntity;
import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import hu.realertek.ManagementSystem.dtos.CaseDto;
import hu.realertek.ManagementSystem.repository.CaseRepository;
import hu.realertek.ManagementSystem.service.interf.AddressService;
import hu.realertek.ManagementSystem.service.interf.BankService;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.service.interf.ConfigService;
import hu.realertek.ManagementSystem.service.interf.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Service
public class CaseServiceImpl implements CaseService {

  @Autowired
  private CaseDtoToCaseEntity caseDtoToCaseEntity;

  @Autowired
  private CaseEntityToCaseDto caseEntityToCaseDto;

  @Autowired
  private CaseRepository caseRepository;

  @Autowired
  private BankService bankService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private UserService userService;

  @Autowired
  private ConfigService configService;

  @Override
  public List<CaseEntity> findAll() {
    return caseRepository.findAll();
  }

  @Override
  public CaseEntity getById(Long id) {
    return caseRepository.getOne(id);
  }

  @Override
  public CaseEntity save(CaseDto caseDto) {
    return caseRepository.save(Objects.requireNonNull(caseDtoToCaseEntity.convert(caseDto)));
  }

  @Override
  public List<CaseEntity> findAllByPreparerOrValuer(UserEntity userEntity) {
    return caseRepository.findAllByPreparerOrValuer(userEntity.getId());
  }

  @Override
  public List<CaseEntity> findAllByPreparerOrValuerWhereState(UserEntity userEntity) {
    return caseRepository.findAllByPreparerOrValuerWhereState(userEntity.getId());
  }

  @Override
  public CaseDto getByIdCaseDto(Long id) {
    return caseEntityToCaseDto.convert(caseRepository.getOne(id));
  }

  @Override
  public void deleteById(Long id) {
    caseRepository.deleteById(id);
  }

  @Override
  public CaseEntity updateState(RealState state, Long id) {
    CaseEntity caseEntity = caseRepository.getOne(id);
    caseEntity.setState(state);
    return caseRepository.save(caseEntity);
  }

  @Override
  public List<CaseEntity> getByBank(BankEntity bankEntity) {
    return caseRepository.getByBank(bankEntity);
  }

  @Override
  public List<CaseEntity> getByValuer(UserEntity userEntity) {
    return caseRepository.getByValuer(userEntity);
  }

  @Override
  public List<CaseEntity> getByPreparer(UserEntity userEntity) {
    return caseRepository.getByPreparer(userEntity);
  }

  @Override
  public List<CaseEntity> getByLoan(ConfigEntity configEntity) {
    return caseRepository.getByLoan(configEntity);
  }

  @Override
  public List<CaseEntity> getByEstimation(ConfigEntity configEntity) {
    return caseRepository.getByEstimation(configEntity);
  }

  @Override
  public CaseEntity updateCase(CaseDto caseDto) {
    CaseEntity caseEntity = caseRepository.getOne(caseDto.getId());
    caseEntity.setId(caseDto.getId());
    caseEntity.setBank(bankService.getByName(caseDto.getBank()));
    caseEntity.setClientName(caseDto.getClientName());
    caseEntity.setReferenceNumber(caseDto.getReferenceNumber());
    addressService.updateAddress(caseDto.getAddress());
    updateUserInCase(caseDto, caseEntity, userService);
    caseEntity.setComment(caseDto.getComment());
    caseEntity.setMoney(caseDto.getMoney());
    if(caseDto.getLoan() == -1) {
      caseEntity.setLoan(null);
    } else {
      caseEntity.setLoan(configService.getById(caseDto.getLoan()));
    }
    if(caseDto.getEstimation() == -1) {
      caseEntity.setEstimation(null);
    } else {
      caseEntity.setEstimation(configService.getById(caseDto.getEstimation()));
    }
    return caseRepository.save(caseEntity);
  }

  public static void updateUserInCase(CaseDto caseDto, CaseEntity caseEntity, UserService userService) {
    caseEntity.setPreparer(userService.getById(userService.getIdByUserFirstAndLastName(caseDto.getPreparer())));
    caseEntity.setValuer(userService.getById(userService.getIdByUserFirstAndLastName(caseDto.getValuer())));
    if(caseDto.getArrived() == "") {
      caseEntity.setArrived(null);
    } else {
      caseEntity.setArrived(LocalDateTime.parse(caseDto.getArrived()));
    }
    if(caseDto.getDeadline() == "") {
      caseEntity.setDeadline(null);
    } else {
      caseEntity.setDeadline(LocalDateTime.parse(caseDto.getDeadline()));
    }
    if(caseDto.getReview() == "") {
      caseEntity.setReview(null);
    } else {
      caseEntity.setReview(LocalDateTime.parse(caseDto.getReview()));
    }
  }

  @Override
  public List<CaseEntity> getByPriority() {
    return caseRepository.getByPriority();
  }

  @Override
  public List<CaseEntity> getByStateDone() {
    return caseRepository.getByStateDone();
  }


  @Override
  public List<CaseEntity> getByStateInProgress() {
    return caseRepository.getByStateInProgress();
  }

  @Override
  public int getCount() {
    return caseRepository.caseCount();
  }

  @Override
  public int getAmount() {
    return caseRepository.getAmount();
  }

  @Override
  public List<Object[]> getStatForCurrentMonth() {
    return caseRepository.getStatForCurrentMonth();
  }

  @Override
  public List<Object[]> getStatBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getStatBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public List<Object[]> getStatForLastThreeMonth() {
    return caseRepository.getStatForLastThreeMonth();
  }

  @Override
  public List<Object[]> getStatForLastSixMonth() {
    return caseRepository.getStatForLastSixMonth();
  }

  @Override
  public List<Object[]> getStatForLastYear() {
    return caseRepository.getStatForLastYear();
  }

  @Override
  public List<Object[]> getCaseCountGroupByBankName() {
    return caseRepository.getCaseCountGroupByBankName();
  }

  @Override
  public List<Object[]> getCaseCountGroupByBankNameBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseCountGroupByBankNameBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public List<Object[]> getCaseCountGroupBySelectedBank(String bank) {
    return caseRepository.getCaseCountGroupBySelectedBank(bankService.getByName(bank).getId());
  }

  @Override
  public int getIktatva() {
    return caseRepository.getIktatva();
  }

  @Override
  public int getIktatvaBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getIktatvaBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getVisszaigazoltBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getVisszaigazoltBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getSzemlezveBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getSzemlezveBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getElokeszitveBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getElokeszitveBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getErtekelesKeszBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getErtekelesKeszBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getVisszaigazolt() {
    return caseRepository.getVisszaigazolt();
  }

  @Override
  public int getSzemlezve() {
    return caseRepository.getSzemlezve();
  }

  @Override
  public int getElokeszitve() {
    return caseRepository.getElokeszitve();
  }

  @Override
  public int getErtekelesKesz() {
    return caseRepository.getErtekelesKesz();
  }

  @Override
  public int moneyForCurrentMonth() {
    Integer moneyForCurrentMonth = caseRepository.moneyForCurrentMonth();
    if(moneyForCurrentMonth == null || moneyForCurrentMonth == 0) {
      return 0;
    } else {
      return caseRepository.moneyForCurrentMonth();
    }
  }

  @Override
  public int getCaseNumberForCurrentMonth() {
    return caseRepository.getCaseNumberForCurrentMonth();
  }

  @Override
  public int getCaseNumberBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseNumberBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getCountBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.caseCountBetweenDates(startLocalDateTime, endLocalDateTime);
  }

  @Override
  public int getAmountBetweenDates(String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return defaultIfNull(caseRepository.getAmountBetweenDates(startLocalDateTime, endLocalDateTime), 0);
  }

  @Override
  public List<CaseEntity> getCasesByValuerForCurrentMonth(Long id, String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCasesByValuerForCurrentMonth(id, startLocalDateTime, endLocalDateTime);
  }

  @Override
  public List<Integer> index(Long id, String start, String end) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    List<CaseEntity> list = caseRepository.getCasesByValuerForCurrentMonth(id, startLocalDateTime, endLocalDateTime);
    List<Integer> returnList = new ArrayList<>();
    if(list.size() > 0) {
      for(int i = 1; i <= list.size(); i++) {
        returnList.add(i);
      }
      return returnList;
    } else {
      returnList.add(0);
      return returnList;
    }
  }

  @Override
  public int sum(Long id, String start, String end) {
    int sum = 0;
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    List<CaseEntity> list = caseRepository.getCasesByValuerForCurrentMonth(id, startLocalDateTime, endLocalDateTime);
    for (CaseEntity caseEntity : list) {
      sum += caseEntity.getMoney();
    }
    return sum;
  }

  @Override
  public List<Object[]> getStatBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getStatBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseCountGroupByBankNameBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getCountBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.caseCountBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getAmountBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return defaultIfNull(caseRepository.getAmountBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank)), 0);
  }

  @Override
  public int getCaseNumberBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseNumberBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getIktatvaBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getIktatvaBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getVisszaigazoltBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getVisszaigazoltBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getSzemlezveBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getSzemlezveBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getElokeszitveBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getElokeszitveBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public int getErtekelesKeszBetweenDatesAndBank(String start, String end, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getErtekelesKeszBetweenDatesAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(bank));
  }

  @Override
  public List<Object[]> getStatBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getStatBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseCountGroupByBankNameBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getCountBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.caseCountBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getAmountBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return defaultIfNull(caseRepository.getAmountBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer)), 0);
  }

  @Override
  public int getCaseNumberBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseNumberBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getIktatvaBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getIktatvaBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getVisszaigazoltBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getVisszaigazoltBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getSzemlezveBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getSzemlezveBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getElokeszitveBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getElokeszitveBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public int getErtekelesKeszBetweenDatesAndValuer(String start, String end, String valuer) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getErtekelesKeszBetweenDatesAndValuer(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer));
  }

  @Override
  public List<Object[]> getStatBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getStatBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseCountGroupByBankNameBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getCountBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.caseCountBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getAmountBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return defaultIfNull(caseRepository.getAmountBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank)), 0);
  }

  @Override
  public int getCaseNumberBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getCaseNumberBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getIktatvaBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getIktatvaBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getVisszaigazoltBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getVisszaigazoltBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getSzemlezveBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getSzemlezveBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getElokeszitveBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getElokeszitveBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

  @Override
  public int getErtekelesKeszBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank) {
    LocalDateTime startLocalDateTime = LocalDateTime.parse(start + "T01:00:00");
    LocalDateTime endLocalDateTime = LocalDateTime.parse(end + "T01:00:00");
    return caseRepository.getErtekelesKeszBetweenDatesAndValuerAndBank(startLocalDateTime, endLocalDateTime, Long.valueOf(valuer), Long.valueOf(bank));
  }

}
