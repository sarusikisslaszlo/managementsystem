package hu.realertek.ManagementSystem.service.impl;

import hu.realertek.ManagementSystem.domain.ConfigEntity;
import hu.realertek.ManagementSystem.dtos.ConfigDto;
import hu.realertek.ManagementSystem.repository.ConfigRepository;
import hu.realertek.ManagementSystem.service.interf.ConfigService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigServiceImpl implements ConfigService {

  @Autowired
  private ConfigRepository configRepository;

  @Override
  public ConfigEntity save(ConfigDto configDto) {
    ConfigEntity configEntity = new ConfigEntity();
    configEntity.setType(configDto.getType());
    configEntity.setValue(configDto.getValue());
    return configRepository.save(configEntity);
  }

  @Override
  public List<ConfigEntity> vtype() {
    return configRepository.vtype();
  }

  @Override
  public List<ConfigEntity> htype() {
    return configRepository.htype();
  }

  @Override
  public ConfigEntity getById(Long id) {
    return configRepository.getOne(id);
  }

  @Override
  public void delete(Long id) {
    configRepository.deleteById(id);
  }

  @Override
  public List<ConfigEntity> findAll() {
    return configRepository.findAll();
  }
}
