package hu.realertek.ManagementSystem.service.impl;

import hu.realertek.ManagementSystem.domain.JobEntity;
import hu.realertek.ManagementSystem.repository.JobRepository;
import hu.realertek.ManagementSystem.service.interf.JobService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobServiceImpl implements JobService {

  @Autowired
  private JobRepository jobRepository;

  @Override
  public List<JobEntity> findAll() {
    return jobRepository.findAll();
  }

  @Override
  public Long getIdByJob(String name) {
    return jobRepository.getIdByJob(name);
  }

  @Override
  public JobEntity getById(Long id) {
    return jobRepository.getOne(id);
  }

}
