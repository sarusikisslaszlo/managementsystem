package hu.realertek.ManagementSystem.service.impl;

import hu.realertek.ManagementSystem.domain.BankEntity;
import hu.realertek.ManagementSystem.dtos.BankDto;
import hu.realertek.ManagementSystem.repository.BankRepository;
import hu.realertek.ManagementSystem.service.interf.BankService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankServiceImpl implements BankService {

  @Autowired
  private BankRepository bankRepository;

  @Override
  public List<BankEntity> findAll() {
    return bankRepository.findAll();
  }

  @Override
  public void deleteById(Long id) {
    bankRepository.deleteById(id);
  }

  @Override
  public BankEntity getById(Long id) {
    return bankRepository.getOne(id);
  }

  @Override
  public BankEntity save(BankDto bankDto) {
    BankEntity bankEntity = new BankEntity();
    bankEntity.setName(bankDto.getName());
    bankEntity.setFileName(bankDto.getFile().getOriginalFilename());
    return bankRepository.save(bankEntity);
  }

  @Override
  public BankEntity getByName(String name) {
    return bankRepository.getByName(name);
  }

}
