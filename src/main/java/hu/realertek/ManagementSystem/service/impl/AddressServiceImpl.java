package hu.realertek.ManagementSystem.service.impl;

import hu.realertek.ManagementSystem.domain.AddressEntity;
import hu.realertek.ManagementSystem.repository.AddressReposirory;
import hu.realertek.ManagementSystem.service.interf.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

  @Autowired
  private AddressReposirory addressReposirory;

  @Override
  public AddressEntity save(AddressEntity addressEntity) {
    return addressReposirory.save(addressEntity);
  }

  @Override
  public AddressEntity getById(Long id) {
    return addressReposirory.getOne(id);
  }

  @Override
  public AddressEntity updateAddress(AddressEntity addressEntity) {
    AddressEntity address = addressReposirory.getOne(addressEntity.getId());
    address.setCity(addressEntity.getCity());
    address.setZipCode(addressEntity.getZipCode());
    address.setStreet(addressEntity.getStreet());
    address.setDoor(addressEntity.getDoor());
    return addressReposirory.save(address);
  }

}
