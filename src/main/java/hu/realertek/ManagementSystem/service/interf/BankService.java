package hu.realertek.ManagementSystem.service.interf;

import hu.realertek.ManagementSystem.domain.BankEntity;
import hu.realertek.ManagementSystem.dtos.BankDto;
import java.util.List;

public interface BankService {

  List<BankEntity> findAll();

  void deleteById(Long id);

  BankEntity getById(Long id);

  BankEntity getByName(String name);

  BankEntity save(BankDto bankDto);
}
