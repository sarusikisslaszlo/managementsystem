package hu.realertek.ManagementSystem.service.interf;

import hu.realertek.ManagementSystem.domain.JobEntity;
import java.util.List;

public interface JobService {

  List<JobEntity> findAll();

  Long getIdByJob(String name);

  JobEntity getById(Long id);

}
