package hu.realertek.ManagementSystem.service.interf;

import hu.realertek.ManagementSystem.domain.BankEntity;
import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.ConfigEntity;
import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import hu.realertek.ManagementSystem.dtos.CaseDto;

import java.text.ParseException;
import java.util.List;

public interface CaseService {

  List<CaseEntity> findAll();
  CaseEntity getById(Long id);
  CaseEntity save(CaseDto caseDto);


  List<CaseEntity> getByBank(BankEntity bankEntity);
  List<CaseEntity> getByValuer(UserEntity userEntity);
  List<CaseEntity> getByPreparer(UserEntity userEntity);
  List<CaseEntity> getByLoan(ConfigEntity configEntity);
  List<CaseEntity> getByEstimation(ConfigEntity configEntity);


  void deleteById(Long id);
  List<CaseEntity> findAllByPreparerOrValuer(UserEntity userEntity);
  List<CaseEntity> findAllByPreparerOrValuerWhereState(UserEntity userEntity);
  CaseDto getByIdCaseDto(Long id);
  List<CaseEntity> getByPriority();
  List<CaseEntity> getCasesByValuerForCurrentMonth(Long id, String start, String end);


  CaseEntity updateCase(CaseDto caseDto);
  CaseEntity updateState(RealState state, Long id);


  int getCount();
  int getAmount();
  List<Object[]> getStatForCurrentMonth();
  List<Object[]> getStatBetweenDates(String start, String end);
  List<Object[]> getStatForLastThreeMonth();
  List<Object[]> getStatForLastSixMonth();
  List<Object[]> getStatForLastYear();
  List<Object[]> getCaseCountGroupByBankName();
  List<Object[]> getCaseCountGroupByBankNameBetweenDates(String start, String end);
  List<Object[]> getCaseCountGroupBySelectedBank(String bank);
  int getIktatva();
  int getIktatvaBetweenDates(String start, String end);
  int getVisszaigazoltBetweenDates(String start, String end);
  int getSzemlezveBetweenDates(String start, String end);
  int getElokeszitveBetweenDates(String start, String end);
  int getErtekelesKeszBetweenDates(String start, String end);
  int getVisszaigazolt();
  int getSzemlezve();
  int getElokeszitve();
  int getErtekelesKesz();
  int moneyForCurrentMonth();
  int getCaseNumberForCurrentMonth();

  int getCaseNumberBetweenDates(String start, String end);

  int getCountBetweenDates(String start, String end);

  int getAmountBetweenDates(String start, String end);


  List<CaseEntity> getByStateDone();
  List<CaseEntity> getByStateInProgress();

  List<Integer> index(Long id, String start, String end);
  int sum(Long id, String start, String end);

  List<Object[]> getStatBetweenDatesAndBank(String start, String end, String bank);
  List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndBank(String start, String end, String bank);
  int getCountBetweenDatesAndBank(String start, String end, String bank);
  int getAmountBetweenDatesAndBank(String start, String end, String bank);
  int getCaseNumberBetweenDatesAndBank(String start, String end, String bank);
  int getIktatvaBetweenDatesAndBank(String start, String end, String bank);
  int getVisszaigazoltBetweenDatesAndBank(String start, String end, String bank);
  int getSzemlezveBetweenDatesAndBank(String start, String end, String bank);
  int getElokeszitveBetweenDatesAndBank(String start, String end, String bank);
  int getErtekelesKeszBetweenDatesAndBank(String start, String end, String bank);

  List<Object[]> getStatBetweenDatesAndValuer(String start, String end, String valuer);
  List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndValuer(String start, String end, String valuer);
  int getCountBetweenDatesAndValuer(String start, String end, String valuer);
  int getAmountBetweenDatesAndValuer(String start, String end, String valuer);
  int getCaseNumberBetweenDatesAndValuer(String start, String end, String valuer);
  int getIktatvaBetweenDatesAndValuer(String start, String end, String valuer);
  int getVisszaigazoltBetweenDatesAndValuer(String start, String end, String valuer);
  int getSzemlezveBetweenDatesAndValuer(String start, String end, String valuer);
  int getElokeszitveBetweenDatesAndValuer(String start, String end, String valuer);
  int getErtekelesKeszBetweenDatesAndValuer(String start, String end, String valuer);

  List<Object[]> getStatBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getCountBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getAmountBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getCaseNumberBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getIktatvaBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getVisszaigazoltBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getSzemlezveBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getElokeszitveBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);
  int getErtekelesKeszBetweenDatesAndValuerAndBank(String start, String end, String valuer, String bank);

}
