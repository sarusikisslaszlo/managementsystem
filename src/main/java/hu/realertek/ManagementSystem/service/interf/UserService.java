package hu.realertek.ManagementSystem.service.interf;

import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.dtos.UsersDto;
import java.util.List;

public interface UserService {

  UserEntity getById(Long id);

  List<UserEntity> findAll();

  void deleteById(Long id);

  UserEntity getByUserName(String username);

  UserEntity getByEmail(String email);

  UsersDto getDtoByEntity(UserEntity userEntity);

  UserEntity save(UserEntity userEntity);

  UserEntity update(UsersDto usersDto);

  UserEntity findByResetPasswordToken(String token);

  UserEntity saveUser(UsersDto usersDto);

  List<UserEntity> getAllValuerByJob();

  List<UserEntity> getAllPreparerByJob();

  Long getIdByUserFirstAndLastName(String name);

  UserEntity updateUser(UsersDto usersDto);

}
