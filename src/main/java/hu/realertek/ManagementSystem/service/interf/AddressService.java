package hu.realertek.ManagementSystem.service.interf;

import hu.realertek.ManagementSystem.domain.AddressEntity;

public interface AddressService {

  AddressEntity save(AddressEntity addressEntity);

  AddressEntity getById(Long id);

  AddressEntity updateAddress(AddressEntity addressEntity);

}
