package hu.realertek.ManagementSystem.service.interf;

import hu.realertek.ManagementSystem.domain.ConfigEntity;
import hu.realertek.ManagementSystem.dtos.ConfigDto;
import java.util.List;

public interface ConfigService {

  ConfigEntity save(ConfigDto configDto);

  List<ConfigEntity> vtype();

  List<ConfigEntity> htype();

  ConfigEntity getById(Long id);

  void delete(Long id);

  List<ConfigEntity> findAll();

}
