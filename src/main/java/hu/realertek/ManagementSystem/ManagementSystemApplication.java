package hu.realertek.ManagementSystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Properties;

@EnableScheduling
@SpringBootApplication
public class ManagementSystemApplication {

	@Autowired
	private ApplicationContext applicationContext;

	public static void main(String[] args) {
		SpringApplication.run(ManagementSystemApplication.class, args);
	}

	@Bean
	public JavaMailSender getJavaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp.mail.yahoo.com");
		mailSender.setPort(587);

		mailSender.setUsername("realertekapp@yahoo.com");
		// TODO át kell írni arra a jelszóra ezt amit majd a yahooval generáltatok
		mailSender.setPassword("Sl729jc@6YryVtV"); // yahoo belejentkezesi jelszo: Sl729jc@6YryVtV

		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.from.email", "realertekapp@yahoo.com");
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");

		return mailSender;
	}

}
