package hu.realertek.ManagementSystem.repository;

import hu.realertek.ManagementSystem.domain.BankEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankRepository extends JpaRepository<BankEntity, Long> {

  BankEntity getByName(String name);

}
