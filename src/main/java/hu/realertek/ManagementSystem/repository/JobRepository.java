package hu.realertek.ManagementSystem.repository;

import hu.realertek.ManagementSystem.domain.JobEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<JobEntity, Long> {

  /**
	 * 
	 * @param name
	 */
	@Query(nativeQuery=true, value="SELECT id FROM munkakor WHERE elnevezes=(:name);")
  Long getIdByJob(@Param("name") String name);

}
