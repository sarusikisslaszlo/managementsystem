package hu.realertek.ManagementSystem.repository;

import hu.realertek.ManagementSystem.domain.BankEntity;
import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.ConfigEntity;
import hu.realertek.ManagementSystem.domain.UserEntity;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends JpaRepository<CaseEntity, Long> {

  List<CaseEntity> getByValuer(UserEntity userEntity);

  List<CaseEntity> getByPreparer(UserEntity userEntity);

  List<CaseEntity> getByBank(BankEntity bankEntity);

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
                                    + "FROM becslesek "
                                    + "WHERE extract(month from erkezett) = extract(month from date_trunc('month', CURRENT_DATE)) "
                                    + "GROUP BY nap "
                                    + "ORDER BY nap;")
  List<Object[]> getStatForCurrentMonth();

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
                                    + "FROM becslesek "
                                    + "WHERE erkezett <= (:end) AND erkezett >= (:start) "
                                    + "GROUP BY nap "
                                    + "ORDER BY nap;")
  List<Object[]> getStatBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
                                    + "FROM becslesek "
                                    + "WHERE erkezett >= current_date - interval '3 month' "
                                    + "AND erkezett < current_date + 1 "
                                    + "GROUP BY nap "
                                    + "ORDER BY nap;")
  List<Object[]> getStatForLastThreeMonth();

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
                                    + "FROM becslesek "
                                    + "WHERE erkezett >= current_date - interval '6 month' "
                                    + "AND erkezett < current_date + 1 "
                                    + "GROUP BY nap "
                                    + "ORDER BY nap;")
  List<Object[]> getStatForLastSixMonth();

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
                                    + "FROM becslesek "
                                    + "WHERE erkezett >= current_date - interval '1 year' "
                                    + "AND erkezett < current_date + 1 "
                                    + "GROUP BY nap "
                                    + "ORDER BY nap;")
  List<Object[]> getStatForLastYear();

  List<CaseEntity> getByLoan(ConfigEntity configEntity);

  List<CaseEntity> getByEstimation(ConfigEntity configEntity);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) AS szum, bank.nev FROM becslesek INNER JOIN bank ON bank.id = becslesek.bank GROUP BY bank.nev;")
  List<Object[]> getCaseCountGroupByBankName();
  @Query(nativeQuery = true, value = "SELECT COUNT(*) AS szum, bank.nev FROM " +
          "(SELECT * FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start)) as becslesek" +
          " INNER JOIN bank ON bank.id = becslesek.bank GROUP BY bank.nev;")
  List<Object[]> getCaseCountGroupByBankNameBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) AS szum, bank.nev FROM becslesek INNER JOIN bank ON bank.id = becslesek.bank GROUP BY bank.nev;")
  List<Object[]> getCaseCountGroupBySelectedBank(@Param("bank") Long bank);

  /**
	 * 
	 * @param id
	 */
	@Query(nativeQuery=true, value="SELECT * FROM becslesek WHERE ertekelo=(:id) AND allapot='ELKULDVE' AND erkezett <= (:end) AND erkezett >= (:start) ORDER BY erkezett ASC;")
  List<CaseEntity> getCasesByValuerForCurrentMonth(@Param("id") Long id, @Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  /**
	 * 
	 * @param id
	 */
	@Query(nativeQuery=true, value="SELECT * FROM becslesek WHERE elokeszito=(:id) OR ertekelo=(:id) ORDER BY hatarido DESC;")
  List<CaseEntity> findAllByPreparerOrValuer(@Param("id") Long id);

  /**
	 * 
	 * @param id
	 */
	@Query(nativeQuery=true, value="SELECT * FROM becslesek WHERE elokeszito=(:id) OR ertekelo=(:id) AND allapot!='ELKULDVE' AND allapot!='VISSZAVONT' ORDER BY hatarido DESC;")
  List<CaseEntity> findAllByPreparerOrValuerWhereState(@Param("id") Long id);

  @Query(nativeQuery = true, value = "SELECT * FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT' AND hatarido >= CURRENT_DATE ORDER BY hatarido ASC LIMIT 10;")
  List<CaseEntity> getByPriority();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT';")
  int caseCount();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start);")
  int caseCountBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT SUM(osszeg) FROM becslesek WHERE allapot!='VISSZAVONT' AND erkezett >= date_trunc('year', Now()) AND erkezett < date_trunc('year', now()) + interval '1 year';")
  int getAmount();

  @Query(nativeQuery = true, value = "SELECT SUM(osszeg) FROM becslesek WHERE allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start);")
  int getAmountBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);


  @Query(nativeQuery = true, value = "SELECT * FROM becslesek WHERE allapot='ELKULDVE' OR allapot='VISSZAVONT';")
  List<CaseEntity> getByStateDone();

  @Query(nativeQuery = true, value = "SELECT * FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT';")
  List<CaseEntity> getByStateInProgress();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='IKTATVA';")
  int getIktatva();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='IKTATVA' AND erkezett <= (:end) AND erkezett >= (:start);")
  int getIktatvaBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='VISSZAIGAZOLT';")
  int getVisszaigazolt();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='VISSZAIGAZOLT' AND erkezett <= (:end) AND erkezett >= (:start);")
  int getVisszaigazoltBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='SZEMLEZVE';")
  int getSzemlezve();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='SZEMLEZVE' AND erkezett <= (:end) AND erkezett >= (:start);")
  int getSzemlezveBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ELOKESZITVE';")
  int getElokeszitve();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ELOKESZITVE' AND erkezett <= (:end) AND erkezett >= (:start);")
  int getElokeszitveBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ERTEKELES_KESZ';")
  int getErtekelesKesz();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ERTEKELES_KESZ' AND erkezett <= (:end) AND erkezett >= (:start);")
  int getErtekelesKeszBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT SUM(osszeg) FROM becslesek WHERE" +
          " allapot!='VISSZAVONT' AND" +
          " extract(month from erkezett) = extract(month from date_trunc('month', CURRENT_DATE)) AND" +
          " extract(year from erkezett) = extract(year from date_trunc('year', CURRENT_DATE));")
  Integer moneyForCurrentMonth();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE" +
          " extract(month from erkezett) = extract(month from date_trunc('month', CURRENT_DATE)) AND" +
          " extract(year from erkezett) = extract(year from date_trunc('year', CURRENT_DATE));")
  int getCaseNumberForCurrentMonth();

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start);")
  int getCaseNumberBetweenDates(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
          + "FROM becslesek "
          + "WHERE erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank) "
          + "GROUP BY nap "
          + "ORDER BY nap;")
  List<Object[]> getStatBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) AS szum, bank.nev FROM " +
          "(SELECT * FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank)) as becslesek" +
          " INNER JOIN bank ON bank.id = becslesek.bank GROUP BY bank.nev;")
  List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int caseCountBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT SUM(osszeg) FROM becslesek WHERE allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getAmountBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getCaseNumberBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='IKTATVA' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getIktatvaBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='VISSZAIGAZOLT' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getVisszaigazoltBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='SZEMLEZVE' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getSzemlezveBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ELOKESZITVE' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getElokeszitveBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ERTEKELES_KESZ' AND erkezett <= (:end) AND erkezett >= (:start) AND bank=(:bank);")
  int getErtekelesKeszBetweenDatesAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
          + "FROM becslesek "
          + "WHERE erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) "
          + "GROUP BY nap "
          + "ORDER BY nap;")
  List<Object[]> getStatBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) AS szum, bank.nev FROM " +
          "(SELECT * FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer)) as becslesek" +
          " INNER JOIN bank ON bank.id = becslesek.bank GROUP BY bank.nev;")
  List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int caseCountBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT SUM(osszeg) FROM becslesek WHERE allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getAmountBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getCaseNumberBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='IKTATVA' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getIktatvaBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='VISSZAIGAZOLT' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getVisszaigazoltBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='SZEMLEZVE' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getSzemlezveBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ELOKESZITVE' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getElokeszitveBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ERTEKELES_KESZ' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer);")
  int getErtekelesKeszBetweenDatesAndValuer(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer);

  @Query(nativeQuery = true, value = "SELECT COUNT(id) AS osszesen, to_char(erkezett, 'YYYY-MM-DD') AS nap "
          + "FROM becslesek "
          + "WHERE erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank) "
          + "GROUP BY nap "
          + "ORDER BY nap;")
  List<Object[]> getStatBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) AS szum, bank.nev FROM " +
          "(SELECT * FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank)) as becslesek" +
          " INNER JOIN bank ON bank.id = becslesek.bank GROUP BY bank.nev;")
  List<Object[]> getCaseCountGroupByBankNameBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot!='ELKULDVE' AND allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int caseCountBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT SUM(osszeg) FROM becslesek WHERE allapot!='VISSZAVONT' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getAmountBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getCaseNumberBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='IKTATVA' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getIktatvaBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='VISSZAIGAZOLT' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getVisszaigazoltBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='SZEMLEZVE' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getSzemlezveBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ELOKESZITVE' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getElokeszitveBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);

  @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM becslesek WHERE allapot='ERTEKELES_KESZ' AND erkezett <= (:end) AND erkezett >= (:start) AND ertekelo=(:valuer) AND bank=(:bank);")
  int getErtekelesKeszBetweenDatesAndValuerAndBank(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("valuer") Long valuer, @Param("bank") Long bank);
}
