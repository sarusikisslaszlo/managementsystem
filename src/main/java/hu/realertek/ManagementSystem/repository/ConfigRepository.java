package hu.realertek.ManagementSystem.repository;

import hu.realertek.ManagementSystem.domain.ConfigEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigRepository extends JpaRepository<ConfigEntity, Long> {

  @Query(nativeQuery = true, value = "SELECT * FROM config WHERE tipus='Értékelés típus';")
  List<ConfigEntity> vtype();

  @Query(nativeQuery = true, value = "SELECT * FROM config WHERE tipus='Hitel típus';")
  List<ConfigEntity> htype();

}
