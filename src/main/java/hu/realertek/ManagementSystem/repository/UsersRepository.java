package hu.realertek.ManagementSystem.repository;

import hu.realertek.ManagementSystem.domain.UserEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<UserEntity, Long> {

  UserEntity getById(Long id);

  UserEntity getByUserName(String username);

  @Query(nativeQuery = true, value = "SELECT * FROM felhasznalo WHERE reset_password_token=(:token);")
  UserEntity findByResetPasswordToken(String token);

  /**
	 * 
	 * @param email
	 */
	@Query(nativeQuery=true, value="SELECT * FROM felhasznalo WHERE email=(:email);")
  UserEntity findByEmail(@Param("email") String email);

  @Query(nativeQuery = true, value = "SELECT * FROM public.felhasznalo, public.munkakor WHERE munkakor=munkakor.id AND munkakor.elnevezes='Értékbecslő';")
  List<UserEntity> getAllValuerByJob();

  @Query(nativeQuery = true, value = "SELECT * FROM public.felhasznalo, public.munkakor WHERE munkakor=munkakor.id AND munkakor.elnevezes='Előkészítő';")
  List<UserEntity> getAllPreparerByJob();

  /**
	 * 
	 * @param name
	 */
	@Query(nativeQuery=true, value="SELECT id FROM felhasznalo WHERE nev=(:name);")
  Long getIdByUserFirstAndLastName(@Param("name") String name);
}
