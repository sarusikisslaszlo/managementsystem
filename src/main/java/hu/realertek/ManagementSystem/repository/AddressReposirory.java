package hu.realertek.ManagementSystem.repository;

import hu.realertek.ManagementSystem.domain.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressReposirory extends JpaRepository<AddressEntity, Long> {

}
