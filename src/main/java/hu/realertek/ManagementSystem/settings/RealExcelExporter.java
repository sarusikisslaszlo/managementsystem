package hu.realertek.ManagementSystem.settings;

import hu.realertek.ManagementSystem.domain.CaseEntity;
import hu.realertek.ManagementSystem.domain.enums.RealState;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RealExcelExporter {

  private static final String EMPTY = "";

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

  @Autowired
  private CaseService caseService;

  private XSSFWorkbook workbook;
  private XSSFSheet sheet;

  private void writeHeaderLine() {
    sheet = workbook.createSheet("Real");

    Row row = sheet.createRow(0);

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setBold(true);
    font.setFontHeight(16);
    style.setFont(font);

    createCell(row, 0, "Állapot", style);
    createCell(row, 1, "Bank", style);
    createCell(row, 2, "Ikatószám", style);
    createCell(row, 3, "Cím", style);
    createCell(row, 4, "Ügyfél név", style);
    createCell(row, 5, "Beérkezett", style);
    createCell(row, 6, "Határidő", style);
    createCell(row, 7, "Szemle", style);
    createCell(row, 8, "Értékbecslő", style);
    createCell(row, 9, "Előkészítő", style);
    createCell(row, 10, "Hitel típus", style);
    createCell(row, 11, "Értékbecslés típus", style);
    createCell(row, 12, "Megjegyzés", style);
    createCell(row, 13, "Összeg", style);
  }

  private void writeHeaderLineforFileSystem() {
    sheet = workbook.createSheet("Real");

    Row row = sheet.createRow(0);

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setBold(true);
    font.setFontHeight(16);
    style.setFont(font);

    createCell(row, 0, "Állapot", style);
    createCell(row, 1, "Bank", style);
    createCell(row, 2, "Ikatószám", style);
    createCell(row, 3, "Cím", style);
    createCell(row, 4, "Ügyfél név", style);
    createCell(row, 5, "Beérkezett", style);
    createCell(row, 6, "Határidő", style);
    createCell(row, 7, "Szemle", style);
    createCell(row, 8, "Értékbecslő", style);
    createCell(row, 9, "Előkészítő", style);
    createCell(row, 10, "Hitel típus", style);
    createCell(row, 11, "Értékbecslés típus", style);
    createCell(row, 12, "Megjegyzés", style);
    createCell(row, 13, "Összeg", style);
    createCell(row, 14, "Létrehozva", style);
  }

  private void writeHeaderLineForUser(List<CaseEntity> list) {
    sheet = workbook.createSheet("Havi elszámolás");

    Row nameRow = sheet.createRow(0);
    Row row = sheet.createRow(1);

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setBold(true);
    font.setFontHeight(16);
    style.setFont(font);

    String name;

    if(list.size() > 0) {
      name = list.get(0).getValuer().getName();
    } else {
      name = "Üres";
    }

    createCell(nameRow, 0, name, style);
    createCell(row, 0, "Bank", style);
    createCell(row, 1, "Dátum", style);
    createCell(row, 2, "Cím", style);
    createCell(row, 3, "Díj", style);
    createCell(row, 4, "Szakértői díj", style);
    createCell(row, 5, "Megjegyzés", style);
  }

  private void createCell(Row row, int columnCount, Object value, CellStyle cellStyle) {
    sheet.autoSizeColumn(columnCount);
    Cell cell = row.createCell(columnCount);
    if(value instanceof Integer) {
      cell.setCellValue((Integer) value);
    } else if(value instanceof Boolean) {
      cell.setCellValue((Boolean) value);
    } else if(value instanceof Long) {
      cell.setCellValue((Long) value);
    } else if(value instanceof RealState) {
      cell.setCellValue(String.valueOf((RealState) value));
    } else if(value instanceof LocalDateTime){
      cell.setCellValue(String.valueOf((LocalDateTime) value));
    } else {
      cell.setCellValue((String) value);
    }
    cell.setCellStyle(cellStyle);
  }

  private void writeDataLines(List<CaseEntity> list) {
    int rowCount = 1;

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setFontHeight(14);
    style.setFont(font);

    for(CaseEntity caseEntity : list) {
      Row row = sheet.createRow(rowCount++);
      int columnCount = 0;

      createCell(row, columnCount++, caseEntity.getState().getDisplayValue(), style);
      createCell(row, columnCount++, caseEntity.getBank().getName(), style);
      createCell(row, columnCount++, caseEntity.getReferenceNumber(), style);
      createCell(row, columnCount++, caseEntity.getAddress().getCity() + " " + caseEntity.getAddress().getZipCode() + ", " + caseEntity.getAddress().getStreet() + ", " + caseEntity.getAddress().getDoor(), style);
      createCell(row, columnCount++, caseEntity.getClientName(), style);
      createCell(row, columnCount++, caseEntity.getArrived().format(formatter), style);
      if(caseEntity.getDeadline() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getDeadline().format(formatter), style);
      }
      if(caseEntity.getReview() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getReview().format(formatter), style);
      }
      createCell(row, columnCount++, caseEntity.getValuer().getName(), style);
      createCell(row, columnCount++, caseEntity.getPreparer().getName(), style);
      if(caseEntity.getLoan() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getLoan().getValue(), style);
      }
      if(caseEntity.getEstimation() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getEstimation().getValue(), style);
      }
      createCell(row, columnCount++, caseEntity.getComment(), style);
      createCell(row, columnCount++, caseEntity.getMoney(), style);
    }
  }

  private void writeDataLinesforFileSystem(List<CaseEntity> list) {
    int rowCount = 1;

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setFontHeight(14);
    style.setFont(font);

    for(CaseEntity caseEntity : list) {
      Row row = sheet.createRow(rowCount++);
      int columnCount = 0;

      createCell(row, columnCount++, caseEntity.getState().getDisplayValue(), style);
      createCell(row, columnCount++, caseEntity.getBank().getName(), style);
      createCell(row, columnCount++, caseEntity.getReferenceNumber(), style);
      createCell(row, columnCount++, caseEntity.getAddress().getCity() + " " + caseEntity.getAddress().getZipCode() + ", " + caseEntity.getAddress().getStreet() + ", " + caseEntity.getAddress().getDoor(), style);
      createCell(row, columnCount++, caseEntity.getClientName(), style);
      createCell(row, columnCount++, caseEntity.getArrived().format(formatter), style);
      if(caseEntity.getDeadline() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getDeadline().format(formatter), style);
      }
      if(caseEntity.getReview() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getReview().format(formatter), style);
      }
      createCell(row, columnCount++, caseEntity.getValuer().getName(), style);
      createCell(row, columnCount++, caseEntity.getPreparer().getName(), style);
      if(caseEntity.getLoan() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getLoan().getValue(), style);
      }
      if(caseEntity.getEstimation() == null) {
        createCell(row, columnCount++, EMPTY, style);
      } else {
        createCell(row, columnCount++, caseEntity.getEstimation().getValue(), style);
      }
      createCell(row, columnCount++, caseEntity.getComment(), style);
      createCell(row, columnCount++, caseEntity.getMoney(), style);
      createCell(row, columnCount++, caseEntity.getCreated(), style);
    }
  }

  private void writeDataLinesForMonthlyWork(List<CaseEntity> list) {
    int rowCount = 2;

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setFontHeight(14);
    style.setFont(font);

    for(CaseEntity caseEntity : list) {
      Row row = sheet.createRow(rowCount++);
      int columnCount = 0;

      createCell(row, columnCount++, caseEntity.getBank().getName(), style);
      createCell(row, columnCount++, caseEntity.getArrived().format(formatter), style);
      createCell(row, columnCount++, caseEntity.getAddress().getCity() + ", " + caseEntity.getAddress().getZipCode() + ", " + caseEntity.getAddress().getStreet() + ", " + caseEntity.getAddress().getDoor(), style);
      createCell(row, columnCount++, caseEntity.getMoney(), style);
      createCell(row, columnCount++, "", style);
      createCell(row, columnCount++, "", style);
    }
  }

  public void exportUserMonthlySum(List<CaseEntity> list, HttpServletResponse response) throws IOException {
    workbook = new XSSFWorkbook();
    writeHeaderLineForUser(list);
    writeDataLinesForMonthlyWork(list);

    ServletOutputStream outputStream = response.getOutputStream();
    workbook.write(outputStream);
    workbook.close();

    outputStream.close();
  }

  public void exportToResponse(List<CaseEntity> list, HttpServletResponse response) throws IOException {
    workbook = new XSSFWorkbook();
    writeHeaderLine();
    writeDataLines(list);

    ServletOutputStream outputStream = response.getOutputStream();
    workbook.write(outputStream);
    workbook.close();

    outputStream.close();
  }

  public void exportToFileSystem(List<CaseEntity> list) throws IOException {
    workbook = new XSSFWorkbook();
    writeHeaderLineforFileSystem();
    writeDataLinesforFileSystem(list);

    DateFormat dateFormatter = new SimpleDateFormat("yyyy_MM_dd");
    String currentDateTime = dateFormatter.format(new Date());

    FileOutputStream outputStream = new FileOutputStream("biztonsagi_becslesek" + currentDateTime + ".xlsx");
    workbook.write(outputStream);
    workbook.close();

    outputStream.close();
  }
}
