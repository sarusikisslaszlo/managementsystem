package hu.realertek.ManagementSystem.settings;

import com.lowagie.text.pdf.BaseFont;
import hu.realertek.ManagementSystem.domain.UserEntity;
import hu.realertek.ManagementSystem.service.interf.CaseService;
import hu.realertek.ManagementSystem.service.interf.UserService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

@Service
public class PdfService {

  private static final String PDF_RESOURCES = "/";  // TODO set path
  private static final String PDF_TEMPLATE = "pdf_valuer_report";
  private static final String PDF_TEMPLATE_STATISTICS = "statistics_report";

  @Autowired
  private CaseService caseService;

  @Autowired
  private UserService userService;

  @Autowired
  private SpringTemplateEngine springTemplateEngine;

  private int[] getDataForChartInteger(List<Object[]> object) {
    int[] tomb = new int[object.size()];
    for (int i = 0; i < object.size(); i++) {
      Object[] tomb2 = object.get(i);
      tomb[i] = ((BigInteger) tomb2[0]).intValue();
    }
    return tomb;
  }

  private String[] getDataForChartString(List<Object[]> object) {
    String[] tomb2 = new String[object.size()];
    for (int i = 0; i < object.size(); i++) {
      Object[] tomb = object.get(i);
      tomb2[i] = tomb[1].toString();
    }
    return tomb2;
  }

  public File generatePdf(Long id, String start, String end) throws IOException, com.lowagie.text.DocumentException {
    Context context = getContext(id, start, end);
    String html = loadAndFillTemplate(context);
    UserEntity userEntity = userService.getById(id);
    return renderPdf(html, userEntity.getName());
  }

  public File generatePdfForStatistics() throws IOException, com.lowagie.text.DocumentException {
    Context context = getContextForStatistics();
    String html = loadAndFillTemplateForStatistics(context);
    return renderPdfForStatistics(html);
  }

  private File renderPdf(String html, String valuerName) throws IOException, com.lowagie.text.DocumentException {
    File file = File.createTempFile("elszamolas_" + valuerName, ".pdf");
    OutputStream outputStream = new FileOutputStream(file);
    ITextRenderer renderer = new ITextRenderer(20f * 4f / 3f, 20);
    ITextFontResolver resolver = renderer.getFontResolver();
    resolver.addFont("C:/kozos/ManagementSystem/font/Calibri_Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED); //    /Users/macbook/test/font/Calibri_Regular.ttf, C:/kozos/ManagementSystem/font/Calibri_Regular.ttf
    renderer.setDocumentFromString(html, new FileSystemResource(PDF_RESOURCES).getURL().toExternalForm());
    renderer.layout();
    renderer.createPDF(outputStream);
    outputStream.close();
    file.deleteOnExit();
    return file;
  }

  private File renderPdfForStatistics(String html) throws IOException, com.lowagie.text.DocumentException {
    File file = File.createTempFile("statisztikak_" + LocalDate.now(), ".pdf");
    OutputStream outputStream = new FileOutputStream(file);
    ITextRenderer renderer = new ITextRenderer(20f * 4f / 3f, 20);
    renderer.setDocumentFromString(html, new FileSystemResource(PDF_RESOURCES).getURL().toExternalForm());
    renderer.layout();
    renderer.createPDF(outputStream);
    outputStream.close();
    file.deleteOnExit();
    return file;
  }

  private Context getContext(Long id, String start, String end) {
    Context context = new Context();
    context.setVariable("report", caseService.getCasesByValuerForCurrentMonth(id, start, end));
    context.setVariable("date", LocalDate.now());
    context.setVariable("sum", caseService.sum(id, start, end));
    context.setVariable("index", caseService.index(id, start, end));
    context.setLocale(Locale.getDefault());
    return context;
  }

  private Context getContextForStatistics() {
    Context context = new Context();
    context.setVariable("count", caseService.getCount());
    context.setVariable("date", LocalDate.now());
    context.setVariable("amount", caseService.getAmount());
    context.setVariable("chartCurrentMonthInteger", getDataForChartInteger(caseService.getStatForCurrentMonth()));
    context.setVariable("chartCurrentMonthString", getDataForChartString(caseService.getStatForCurrentMonth()));
    context.setLocale(Locale.getDefault());
    return context;
  }

  private String loadAndFillTemplate(Context context) {
    return springTemplateEngine.process(PDF_TEMPLATE, context);
  }

  private String loadAndFillTemplateForStatistics(Context context) {
    return springTemplateEngine.process(PDF_TEMPLATE_STATISTICS, context);
  }

}
